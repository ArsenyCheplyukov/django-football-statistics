from django.core.management.base import BaseCommand
from stats.background_workers.generate_match import generate_random_match_data


class Command(BaseCommand):
    help = 'Generate random match data'

    def handle(self, *args, **options):
        json_data = generate_random_match_data()
        self.stdout.write(json_data)

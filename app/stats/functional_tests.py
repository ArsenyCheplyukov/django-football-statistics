"""
Functional tests (testing functionality as user)
"""
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from time import sleep


class TestListView(unittest.TestCase):
    """Test generic list view."""
    base_url = "http://app:8000"

    def setUp(self) -> None:
        """Browser automation setup"""
        chrome_options = Options()
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")
        # chrome_options.add_argument('--disable-blink-features=AutomationControlled')
        self.chrome = webdriver.Remote(
            command_executor='http://headless_chrome:3000/webdriver',
            # command_executor="http://localhost:4444/wd/hub",
            options=chrome_options,
        )
        self.chrome.implicitly_wait(2)

    def test_full_interaction(self):
        """Test full interaction with the website"""
        # Go to the base URL
        self.chrome.get(self.base_url)
        self.assertEqual(self.chrome.title, "Main page")

        # Wait for the first link in the sidebar to be clickable
        WebDriverWait(self.chrome, 10).until(EC.element_to_be_clickable(
            (By.CSS_SELECTOR, ".sidebar li a")
        ))
        first_link = self.chrome.find_element(By.CSS_SELECTOR, ".sidebar li a")
        self.assertEqual(first_link.text, "Team List")
        first_link.click()

        self.assertEqual(self.chrome.current_url,
                         f"{self.base_url}/list/team/")

        # wait until team list is loaded
        sleep(4)

        # # some strange error with unclickable element
        # loaded_first_page = self.chrome.find_element(By.XPATH,
        #                                              "//a[@href='?page=2']")
        # loaded_first_page.click()

        # this is alternative
        self.chrome.get(f"{self.base_url}/list/team/?page=2")

        # wait until page is loaded
        sleep(4)
        self.assertEqual(self.chrome.current_url,
                         f"{self.base_url}/list/team/?page=2")

        # Wait for the second page link to be clickable
        # WebDriverWait(self.chrome, 10).until(EC.element_to_be_clickable(
        #     (By.CSS_SELECTOR, "li.list-group-item a")
        # ))
        second_page_link = self.chrome.find_elements(By.CSS_SELECTOR,
                                                     "li.list-group-item a")[0]
        second_page_link.click()

        # wait until page is loaded
        sleep(2)
        name_link = self.chrome.find_element(
            By.CSS_SELECTOR,
            "section.single-element-section .container"
        )
        self.assertIn("Team", name_link.text.split("\n"))

        # # Wait for the ball logo to be clickable
        WebDriverWait(self.chrome, 10).until(EC.element_to_be_clickable(
            (By.CSS_SELECTOR, "svg")
        ))
        ball_logo = self.chrome.find_element(By.CSS_SELECTOR, "svg")
        ball_logo.click()

        # Assert that we are back on the home page
        self.assertEqual(self.chrome.current_url, f"{self.base_url}/")

    def tearDown(self) -> None:
        """Exit from browser instance"""
        self.chrome.quit()


if __name__ == '__main__':
    unittest.main(warnings='ignore')

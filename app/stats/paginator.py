"""
All staff related to pagination
"""
from typing import Dict, Any, Iterable, Protocol
from funcy import (
    chunks, first, second, last, nth
)


class BasePaginationStrategy(Protocol):
    """Base class for pagination strategies."""

    def __init__(self, data: list,
                 current_page: int = 0,
                 elem_per_page: int = 10,
                 maximum_shown: int = 10):
        """Initialize the pagination strategy."""
        pass

    def get_page(self, page_number: int) -> Dict[str, Any]:
        """Get page the data."""
        pass

    def next(self) -> Dict[str, Any]:
        """Move to the next page."""
        pass

    def previous(self) -> Dict[str, Any]:
        """Move to the previous page."""
        pass

    def first(self) -> Dict[str, Any]:
        """Move to the first page."""
        pass

    def second(self) -> Dict[str, Any]:
        """Move to the second page."""
        pass

    def prelast(self) -> Dict[str, Any]:
        """Move to the prelast page."""
        pass

    def last(self) -> Dict[str, Any]:
        """Move to the last page."""
        pass


class Paginator(BasePaginationStrategy):
    """Basic paginator strategy."""
    def __init__(self, data: Iterable,
                 current_page: int = 0,
                 elem_per_page: int = 10,
                 maximum_shown: int = 5):
        """Initialize the basic pagination strategy."""
        self.current_page = current_page
        self.elements_per_page = elem_per_page
        self.maximum_shown = maximum_shown
        self.__data = list(chunks(elem_per_page, data))
        self.total_pages = len(self.__data)

    def _get_page(self, data) -> Dict[str, Any]:
        """Get the page data."""
        return {
            "current_page": self.current_page,
            "total_pages": self.total_pages,
            "page_range": self.get_page_range(),
            "has_next": self.has_next(),
            "has_previous": self.has_previous(),
            "data": data
        }

    def has_next(self) -> bool:
        return self.current_page < self.total_pages - 1

    def has_previous(self) -> bool:
        return self.current_page > 0

    def get_page_range(self):
        half_shown = (self.maximum_shown - 2) // 2
        first_page = max(1, self.current_page - half_shown)
        last_page = min(self.total_pages, self.current_page + half_shown)

        if last_page - first_page + 1 < self.maximum_shown - 2:
            # Adjust the range if there are fewer pages than the maximum shown
            diff = self.maximum_shown - (last_page - first_page + 1)
            if first_page > 1:
                first_page = max(1, first_page - diff)
            else:
                last_page = min(self.total_pages, last_page + diff)

        # Generate the page range
        page_range = list(range(first_page - 1, self.current_page)) + \
            [self.current_page] + \
            list(range(self.current_page + 1, last_page))

        return page_range

    def get_page(self, page_number: int) -> Dict[str, Any]:
        if page_number > self.total_pages - 1:
            self.current_page = 0
        else:
            self.current_page = page_number
        return self._get_page(nth(self.current_page, self.__data))

    def next(self) -> Dict[str, Any]:
        """Move to the next page."""
        self.current_page = min(self.current_page + 1, self.total_pages - 1)
        return self._get_page(nth(self.current_page, self.__data))

    def previous(self) -> Dict[str, Any]:
        """Move to the previous page."""
        self.current_page = max(self.current_page - 1, 0)
        return self._get_page(nth(self.current_page, self.__data))

    def first(self) -> Dict[str, Any]:
        """Move to the first page."""
        self.current_page = 0
        return self._get_page(first(self.__data))

    def second(self) -> Dict[str, Any]:
        """Move to the second page."""
        self.current_page = min(1, self.total_pages - 1)
        return self._get_page(second(self.__data))

    def prelast(self) -> Dict[str, Any]:
        """Move to the prelast page."""
        self.current_page = max(self.total_pages - 2, 0)
        return self._get_page(nth(-2, self.__data))

    def last(self) -> Dict[str, Any]:
        """Move to the last page."""
        self.current_page = self.total_pages - 1
        return self._get_page(last(self.__data))

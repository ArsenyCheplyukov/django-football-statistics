"""
Test result of common queries
"""
from django.test import TestCase, override_settings

from decimal import Decimal

from stats.models import (
    Country,
    League,
    Team,
    Player,
    PlayerAttributes,
    Match
)

from stats.queries import (
    ConcreteTeamStatistics,
    ListTeamStatistics,
    ConcretePlayerStatistics,
    ListPlayerStatistics,
    ConcreteLeagueStatistics,
    ListLeagueStatistics,
    ConcreteMatchStatistics,
    ListMatchStatistics,
    ConcreteCountryStatistics,
    ListCountryStatistics,
    ListStatisticsFactory,
    ConcreteStatisticsFactory,
)


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class ConcreteTeamTesting(TestCase):
    def setUp(self):
        country = Country.objects.create(name='Atlantis')
        league = League.objects.create(name='Gridiron Gods', country=country)
        # Create two teams
        self.team = Team.objects.create(team_api_id=0, team_fifa_api_id=0,
                                        team_long_name='Team 0',
                                        team_short_name='T0')
        team_2 = Team.objects.create(team_api_id=1, team_fifa_api_id=1,
                                     team_long_name='Team 1',
                                     team_short_name='T1')
        # Create a match associated with country, league, and teams
        self.match = [
            Match.objects.create(
                country=country,
                league=league,
                season='Winter',
                stage=36,
                date='2010-01-01',
                match_api_id=0,
                home_team_api=self.team,
                away_team_api=team_2,
                home_team_goal=3,
                away_team_goal=2
            ),
            Match.objects.create(
                country=country,
                league=league,
                season='Summer',
                stage=36,
                date='2010-01-01',
                match_api_id=1,
                home_team_api=team_2,
                away_team_api=self.team,
                home_team_goal=3,
                away_team_goal=2
            )
        ]
        self.statistics = ConcreteTeamStatistics(self.team.id)

    def test_init(self):
        self.assertEqual(self.statistics.filtered_query_id,
                         self.team.team_api_id)

    def test_get_current_object_fields(self):
        fields = self.statistics.get_current_object_fields()
        self.assertEqual(fields.values[2:], [0, 'Team 0', 'T0'])
        self.assertEqual(fields.names, ['Id',
                                        'Team api id',
                                        'Team fifa api id',
                                        'Team long name',
                                        'Team short name'])

    def test_total_matches_played(self):
        self.assertEqual(self.statistics.total_matches_played().value, 2)

    def test_win_loss_draw_count(self):
        self.assertEqual(
            self.statistics.win_loss_draw_count().value, (1, 1, 0)
        )

    def test_goals_scored_and_conceded(self):
        self.assertEqual(
            self.statistics.goals_scored_and_conceded().value, (5, 5)
        )

    def test_avg_goals_per_match(self):
        self.assertEqual(
            self.statistics.avg_goals_per_match().value, (2.5, 2.5)
        )


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class ListTeamStatisticsTesting(TestCase):
    def setUp(self):
        country = Country.objects.create(name='Atlantis')
        league = League.objects.create(name='Gridiron Gods', country=country)
        # Create two teams
        self.team = [
            Team.objects.create(team_api_id=0, team_fifa_api_id=0,
                                team_long_name='Team 0',
                                team_short_name='T0'),
            Team.objects.create(team_api_id=1, team_fifa_api_id=1,
                                team_long_name='Team 1',
                                team_short_name='T1'),
            Team.objects.create(team_api_id=2, team_fifa_api_id=2,
                                team_long_name='Team 2',
                                team_short_name='T2')
        ]
        # Create a match associated with country, league, and teams
        self.match = [
            Match.objects.create(
                country=country,
                league=league,
                season='Winter',
                stage=36,
                date='2010-01-01',
                match_api_id=0,
                home_team_api=self.team[0],
                away_team_api=self.team[1],
                home_team_goal=3,
                away_team_goal=2
            ),
            Match.objects.create(
                country=country,
                league=league,
                season='Summer',
                stage=36,
                date='2010-02-02',
                match_api_id=1,
                home_team_api=self.team[1],
                away_team_api=self.team[0],
                home_team_goal=3,
                away_team_goal=2
            )
        ]
        self.statistics = ListTeamStatistics()

    def test_display_list(self):
        self.assertEqual(len(self.statistics.display_list()), 3)

    def test_range_by_total_matches_played(self):
        fields = self.statistics.range_by_total_matches_played()
        self.assertEqual(fields.x_data, ['Team 0', 'Team 1'])
        self.assertEqual(fields.y_data, [2, 2])


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class ConcretePlayerTest(TestCase):
    def setUp(self):
        self.players = [
            Player.objects.create(
                player_api_id=i,
                player_name=f'Player {i}',
                player_fifa_api_id=i,
                birthday=f'199{i}-0{i+1}-0{i+1}',
                height=180,
                weight=74,
            )
            for i in range(3)
        ]
        self.player_attributes = [
            PlayerAttributes.objects.create(
                player_api_id=self.players[0].player_fifa_api_id,
                player_api=self.players[0],
                date=f'201{i}-01-01',
                overall_rating=50+i*10
            )
            for i in range(3)
        ]
        self.side_player_attributes = [
            PlayerAttributes.objects.create(
                player_api_id=self.players[i+1].player_fifa_api_id,
                player_api=self.players[i+1],
                date=f'201{i}-01-01',
                overall_rating=50+i*10
            )
            for i in range(2)]
        self.statistics = ConcretePlayerStatistics(self.players[0].id)

    def test_init(self):
        self.assertEqual(self.statistics.player_api_id,
                         self.players[0].player_api_id)

    def test_get_current_object_fields(self):
        fields = self.statistics.get_current_object_fields()
        self.assertEqual(fields.names, ['Id', 'Player api id', 'Player name',
                                        'Player fifa api id', 'Birthday',
                                        'Height', 'Weight'])
        self.assertEqual(fields.values[1:], [0, 'Player 0', 0, '1990-01-01',
                                             180, 74])

    def test_player_growth(self):
        growth = self.statistics.player_growth()
        self.assertEqual(growth.x_data, [f'201{i}-01-01' for i in range(3)])
        self.assertEqual(growth.y_data, [50+i*10 for i in range(3)])


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class ListPlayerStatisticsTest(TestCase):
    def setUp(self):
        self.players = [
            Player.objects.create(
                player_api_id=i,
                player_name=f'Player {i}',
                player_fifa_api_id=i,
                birthday=f'199{i}-0{i+1}-0{i+1}',
                height=180,
                weight=74,
            )
            for i in range(3)
        ]
        self.player_attributes = [[
                PlayerAttributes.objects.create(
                    player_api_id=self.players[j].player_fifa_api_id,
                    player_api=self.players[j],
                    date=f'201{i}-01-01',
                    overall_rating=50+j*10+i*5,
                    short_passing=(20+j*10+i),
                    long_passing=(30+j*10+i),
                    vision=(40+j*10+i),
                    crossing=(50+j*10+i),
                    gk_diving=20+j*10+i,
                    gk_handling=30+j*10+i,
                    gk_kicking=40+j*10+i,
                    gk_positioning=50+j*10+i,
                    gk_reflexes=60+j*10+i,
                    finishing=50+j*10
                )
                for i in range(3)
            ] for j in range(3)
        ]
        self.statistics = ListPlayerStatistics()

    def test_display_list(self):
        self.assertEqual(len(self.statistics.display_list()), 3)

    def test_overall_rating_distribution(self):
        overall_score = self.statistics.overall_rating_distribution()
        self.assertEqual(overall_score.x_data, [80, 75, 70, 65, 60, 55, 50])
        self.assertEqual(overall_score.y_data, [1, 1, 2, 1, 2, 1, 1])

    def test_player_average_rating(self):
        average_rating = self.statistics.player_average_rating()
        self.assertEqual(average_rating.x_data, ['Player 2', 'Player 1',
                                                 'Player 0'])
        self.assertEqual(average_rating.y_data, [75, 65, 55])

    def test_top_goal_scorers(self):
        top_scorers = self.statistics.top_goal_scorers()
        self.assertEqual(top_scorers.x_data, ['Player 2', 'Player 1',
                                              'Player 0'])
        self.assertEqual(top_scorers.y_data, [70, 60, 50])

    def test_players_with_most_assists(self):
        top = self.statistics.players_with_most_assists()
        self.assertEqual(top.x_data, ['Player 2', 'Player 1', 'Player 0'])
        self.assertEqual([round(i, 2) for i in top.y_data], [Decimal('44.80'),
                                                             Decimal('36.80'),
                                                             Decimal('28.80')])

    def test_players_with_most_clean_sheets(self):
        top = self.statistics.players_with_most_clean_sheets()
        self.assertEqual(top.x_data, ['Player 2', 'Player 1', 'Player 0'])
        self.assertEqual(top.y_data, [61.00, 51.00, 41.00])


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class ConcreteLeagueStatisticsTesting(TestCase):
    def setUp(self):
        self.country = Country.objects.create(
            name='Country 0'
        )
        self.leagues = [
            League.objects.create(
                country=self.country,
                name=f'League {i}'
            )
            for i in range(3)
        ]
        self.teams = [
            Team.objects.create(
                team_api_id=i,
                team_fifa_api_id=i,
                team_long_name=f'Team {i}',
                team_short_name=f'T{i}'
            )
            for i in range(6)
        ]
        self.match = [[
                Match.objects.create(
                    country=self.country,
                    league=self.leagues[j],
                    date=f'201{j}-0{i}-0{i}',
                    match_api_id=j*5+i,
                    home_team_api=self.teams[i],
                    away_team_api=self.teams[i+1],
                    home_team_goal=1,
                    away_team_goal=i
                )
                for i in range(5)]
            for j in range(2)
        ]
        self.statistics = ConcreteLeagueStatistics(self.leagues[0].id)

    def test_get_current_object_fields(self):
        fields = self.statistics.get_current_object_fields()
        self.assertEqual(fields.values[1:], ['League 0'])
        self.assertEqual(fields.names, ['Id', 'Name'])

    def test_table_standings(self):
        standings = self.statistics.table_standings()
        self.assertEqual(standings.x_data, ['T4 vs T5', 'T3 vs T4', 'T2 vs T3',
                                            'T1 vs T2', 'T0 vs T1'])
        self.assertEqual(standings.y_data, [3, 2, 1, 0, 1])

    def test_top_teams_by_points(self):
        top = self.statistics.top_teams_by_points()
        self.assertEqual(top.x_data, ['Team 5', 'Team 4', 'Team 3', 'Team 0',
                                      'Team 1', 'Team 2'])
        self.assertEqual(top.y_data, [4, 3, 2, 1, 0, 0])

    def test_goal_difference_distribution(self):
        diff = self.statistics.goal_difference_distribution()
        self.assertEqual(diff.x_data, [3, 2, 1, 0])
        self.assertEqual(diff.y_data, [1, 1, 2, 1])


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class ListLeagueStatisticsTesting(TestCase):
    def setUp(self):
        self.country = Country.objects.create(
            name='Country 0'
        )
        self.leagues = [
            League.objects.create(
                country=self.country,
                name=f'League {i}'
            )
            for i in range(3)
        ]
        self.teams = [
            Team.objects.create(
                team_api_id=i,
                team_fifa_api_id=i,
                team_long_name=f'Team {i}',
                team_short_name=f'T{i}'
            )
            for i in range(6)
        ]
        self.match = [[
                Match.objects.create(
                    country=self.country,
                    league=self.leagues[j],
                    date=f'201{j}-0{i}-0{i}',
                    match_api_id=j*5+i,
                    home_team_api=self.teams[i],
                    away_team_api=self.teams[i+1],
                    home_team_goal=1,
                    away_team_goal=i
                )
                for i in range(5)]
            for j in range(2)
        ]
        self.statistics = ListLeagueStatistics()

    def test_display_list(self):
        self.assertEqual(len(self.statistics.display_list()), 3)

    def test_most_matches_played(self):
        played = self.statistics.most_matches_played()
        self.assertEqual(played.x_data, ['League 1', 'League 0'])
        self.assertEqual(played.y_data, [5, 5])


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class ConcreteMatchStatisticsTesting(TestCase):
    def setUp(self):
        self.country = Country.objects.create(
            name='Country 0'
        )
        self.leagues = [
            League.objects.create(
                country=self.country,
                name=f'League {i}'
            )
            for i in range(3)
        ]
        self.teams = [
            Team.objects.create(
                team_api_id=i,
                team_fifa_api_id=i,
                team_long_name=f'Team {i}',
                team_short_name=f'T{i}'
            )
            for i in range(6)
        ]
        self.match = [[
                Match.objects.create(
                    country=self.country,
                    league=self.leagues[j],
                    date=f'201{j}-0{i+1}-0{i+1}',
                    match_api_id=j*5+i,
                    home_team_api=self.teams[i],
                    away_team_api=self.teams[i+1],
                    home_team_goal=1,
                    away_team_goal=i,
                )
                for i in range(5)]
            for j in range(2)
        ]
        self.statistics = ConcreteMatchStatistics(self.match[0][0].id)

    def test_get_current_object_fields(self):
        fields = self.statistics.get_current_object_fields()
        self.assertEqual(fields.names, ['Id', 'Season', 'Stage', 'Date',
                                        'Match api id', 'Home team goal',
                                        'Away team goal'])
        self.assertEqual(fields.values[1:], [None, None, '2010-01-01', 0,
                                             1, 0])


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class ListMatchStatisticsTesting(TestCase):
    def setUp(self):
        self.country = Country.objects.create(
            name='Country 0'
        )
        self.leagues = [
            League.objects.create(
                country=self.country,
                name=f'League {i}'
            )
            for i in range(3)
        ]
        self.teams = [
            Team.objects.create(
                team_api_id=i,
                team_fifa_api_id=i,
                team_long_name=f'Team {i}',
                team_short_name=f'T{i}'
            )
            for i in range(6)
        ]
        self.match = [[
                Match.objects.create(
                    country=self.country,
                    league=self.leagues[j],
                    date=f'201{j}-0{i+1}-0{i+1}',
                    match_api_id=j*5+i,
                    home_team_api=self.teams[i],
                    away_team_api=self.teams[i+1],
                    home_team_goal=j+1,
                    away_team_goal=i+1,
                )
                for i in range(5)]
            for j in range(2)
        ]
        self.statistics = ListMatchStatistics()

    def test_display_list(self):
        self.assertEqual(len(self.statistics.display_list()), 10)

    def test_avg_goals_per_match(self):
        avg = self.statistics.avg_goals_per_match()
        self.assertEqual(avg.value, 4.5)

    def test_goal_distribution_in_time_intervals(self):
        distribution = self.statistics.goal_distribution_in_time_intervals()
        self.assertEqual(distribution.x_data, [2011, 2010])
        self.assertEqual(distribution.y_data, [5, 4])

    def test_matches_with_most_goals(self):
        matches = self.statistics.matches_with_most_goals()
        self.assertEqual(matches.x_data, ['2010) T4 vs T5', '2011) T4 vs T5'])
        self.assertEqual(matches.y_data, [6, 7])


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class ConcreteCountryStatisticsTesting(TestCase):
    def setUp(self):
        self.countries = [
            Country.objects.create(
                name=f'Country {i+1}'
            )
            for i in range(3)
        ]
        self.statstics = ConcreteCountryStatistics(self.countries[0].id)

    def test_get_current_object_fields(self):
        fields = self.statstics.get_current_object_fields()
        self.assertEqual(fields.names, ['Id', 'Name'])
        self.assertEqual(fields.values[1:], ['Country 1'])


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class ListCountryStatisticsTesting(TestCase):
    def setUp(self):
        self.countries = [
            Country.objects.create(
                name=f'Country {i+1}'
            )
            for i in range(3)
        ]
        self.leagues = [
            League.objects.create(
                country=self.countries[i],
                name=f'League {i}'
            )
            for i in range(3)
        ]
        self.teams = [
            Team.objects.create(
                team_api_id=i,
                team_fifa_api_id=i,
                team_long_name=f'Team {i}',
                team_short_name=f'T{i}'
            )
            for i in range(4)
        ]
        self.match = [[
                Match.objects.create(
                    country=self.countries[i],
                    league=self.leagues[j],
                    date=f'201{j}-0{i+1}-0{i+1}',
                    match_api_id=j*3+i,
                    home_team_api=self.teams[i],
                    away_team_api=self.teams[i+1],
                    home_team_goal=j+1,
                    away_team_goal=i+1,
                )
                for i in range(3)]
            for j in range(3)
        ]
        self.statistics = ListCountryStatistics()

    def test_display_list(self):
        self.assertEqual(len(self.statistics.display_list()), 3)

    def test_overall_performance_comparison(self):
        performance = self.statistics.overall_performance_comparison()
        self.assertEqual(performance.x_data, ['Country 1', 'Country 2',
                                              'Country 3'])
        # sum of goals
        self.assertEqual(performance.y_data, [9, 12, 15])


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class TestStatisticsFactories(TestCase):
    def test_list_statistics_factory_invalid_type(self):
        # Test invalid stats type
        with self.assertRaises(KeyError):
            ListStatisticsFactory(stats_type="invalid_type")

    def test_list_statistics_factory_wrong_type(self):
        # Test wrong type for stats type
        with self.assertRaises(KeyError):
            ListStatisticsFactory(stats_type=123)

    def test_concrete_statistics_factory_invalid_type(self):
        # Test invalid stats type
        with self.assertRaises(KeyError):
            ConcreteStatisticsFactory(stats_type="invalid_type", id=1)

    def test_concrete_statistics_factory_out_of_index(self):
        # Test stats type out of indexes
        with self.assertRaises(Country.DoesNotExist):
            ConcreteStatisticsFactory(stats_type="country",
                                      id=1000)

    def test_concrete_statistics_factory_wrong_type(self):
        # Test wrong type for stats type
        with self.assertRaises(KeyError):
            ConcreteStatisticsFactory(stats_type=123, id=1)

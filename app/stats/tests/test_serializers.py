"""
Test application serializers
"""
from django.test import TestCase, override_settings
from rest_framework.exceptions import ValidationError
from stats.serializers import (
    GraphSerializer,
    SingleDataListSerializer,
    CustomValueSerializer,
    ListDataListsSerializer
)

from stats.datatypes import (
    Graph,
    Histogram,
    SingleDataList,
    ListDataLists,
    CustomValue
)


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class TestGraphSerializer(TestCase):
    """Test data graph and hist serializers (they have same fields)"""
    def test_valid_graph_serializer(self):
        """Test on valid data."""
        data = {
            'name': 'Test Graph',
            'x_axis': 'X Axis',
            'y_axis': 'Y Axis',
            'x_data': [1, 2, 3],
            'y_data': [4.5, 5.6, 6.7],
            'legends': ['Legend 1', 'Legend 2', 'Legend 3']
        }
        serializer = GraphSerializer(data=data)
        self.assertTrue(serializer.is_valid())

    def test_invalid_graph_serializer(self):
        """ Test on invalid data - Missing required field"""
        data = {
            'name': 'Test Graph',
            'x_axis': 'X Axis',
            'y_axis': 'Y Axis',
            'x_data': [1, 2, 3],
            # Missing 'y_data' field
        }
        serializer = GraphSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn('y_data', serializer.errors)

    def test_invalid_legends(self):
        """Test of invalid data - Legends not a list"""
        data = {
            'name': 'Test Graph',
            'x_axis': 'X Axis',
            'y_axis': 'Y Axis',
            'x_data': [1, 2, 3],
            'y_data': [4.5, 5.6, 6.7],
            'legends': 'Invalid Legends'  # Should be a list
        }
        serializer = GraphSerializer(data=data)
        with self.assertRaises(ValidationError):
            serializer.is_valid(raise_exception=True)

    def test_graph_to_representation(self):
        """Test to_representation method for Graph."""
        graph = Graph(
            name='Test Graph',
            x_axis='X Axis',
            y_axis='Y Axis',
            x_data=[1, 2, 3],
            y_data=[4.5, 5.6, 6.7],
            legends=['Legend 1', 'Legend 2', 'Legend 3']
        )
        serializer = GraphSerializer(instance=graph)
        expected_representation = {
            'name': 'Test Graph',
            'x_axis': 'X Axis',
            'y_axis': 'Y Axis',
            'x_data': [1, 2, 3],
            'y_data': [4.5, 5.6, 6.7],
            'legends': ['Legend 1', 'Legend 2', 'Legend 3']
        }
        self.assertEqual(serializer.data, expected_representation)

    def test_histogram_to_representation(self):
        """Test to_representation method for Graph."""
        histogram = Histogram(
            name='Test Graph',
            x_axis='X Axis',
            y_axis='Y Axis',
            x_data=[1, 2, 3],
            y_data=[4.5, 5.6, 6.7],
            legends=['Legend 1', 'Legend 2', 'Legend 3']
        )
        serializer = GraphSerializer(instance=histogram)
        expected_representation = {
            'name': 'Test Graph',
            'x_axis': 'X Axis',
            'y_axis': 'Y Axis',
            'x_data': [1, 2, 3],
            'y_data': [4.5, 5.6, 6.7],
            'legends': ['Legend 1', 'Legend 2', 'Legend 3']
        }
        self.assertEqual(serializer.data, expected_representation)


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class TestSingleDataClass(TestCase):
    """Test serializer for single data class"""
    def test_valid_single_data_list_serializer(self):
        """Test on valid data"""
        data = {
            'values': ['Value 1', 'Value 2', 'Value 3'],
            'names': ['Name 1', 'Name 2', 'Name 3']
        }
        serializer = SingleDataListSerializer(data=data)
        self.assertTrue(serializer.is_valid())

    def test_missing_values_field(self):
        """Test case for missing 'values' field."""
        data = {
            'names': ['Name 1', 'Name 2', 'Name 3']
        }
        serializer = SingleDataListSerializer(data=data)
        self.assertFalse(serializer.is_valid())

    def test_missing_names_field(self):
        """Test case for missing 'names' field."""
        data = {
            'values': ['Value 1', 'Value 2', 'Value 3']
        }
        serializer = SingleDataListSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn('names', serializer.errors)

    def test_invalid_values_type(self):
        """Test case for invalid type of 'values' field."""
        data = {
            'values': 'Invalid Values',
            'names': ['Name 1', 'Name 2', 'Name 3']
        }
        serializer = SingleDataListSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn('values', serializer.errors)

    def test_invalid_names_type(self):
        """Test case for invalid type of 'names' field."""
        data = {
            'values': ['Value 1', 'Value 2', 'Value 3'],
            'names': {'Name 1', 'Name 2', 'Name 3'}  # Should be a list
        }
        serializer = SingleDataListSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn('names', serializer.errors)

    def test_empty_values_list(self):
        """Test case for empty 'values' list."""
        data = {
            'values': [],
            'names': ['Name 1', 'Name 2', 'Name 3']
        }
        serializer = SingleDataListSerializer(data=data)
        self.assertFalse(serializer.is_valid())  # Empty list is valid

    def test_empty_names_list(self):
        """Test case for empty 'names' list."""
        data = {
            'values': ['Value 1', 'Value 2', 'Value 3'],
            'names': []
        }
        serializer = SingleDataListSerializer(data=data)
        self.assertFalse(serializer.is_valid())  # Empty list is valid

    def test_single_data_list_to_representation(self):
        """Test to_representation method for SingleDataList."""
        single_data_list = SingleDataList(
            values=['Value 1', 'Value 2', 'Value 3'],
            names=['Name 1', 'Name 2', 'Name 3']
        )
        serializer = SingleDataListSerializer(instance=single_data_list)
        expected_representation = {
            'values': ['Value 1', 'Value 2', 'Value 3'],
            'names': ['Name 1', 'Name 2', 'Name 3']
        }
        self.assertEqual(serializer.data, expected_representation)


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class TestCustomValueSerializer(TestCase):
    """Test serializer for custom value datatype."""
    def test_valid_custom_value_serializer(self):
        """Valid data."""
        data = {
            'text': 'Test Text',
            'value': 123
        }
        serializer = CustomValueSerializer(data=data)
        self.assertTrue(serializer.is_valid())

    def test_missing_text_field(self):
        """Test case for missing 'text' field."""
        data = {
            'value': 123
        }
        serializer = CustomValueSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn('text', serializer.errors)

    def test_missing_value_field(self):
        """Test case for missing 'value' field."""
        data = {
            'text': 'Test Text'
        }
        serializer = CustomValueSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn('value', serializer.errors)

    def test_invalid_text_type(self):
        """Test case for invalid type of 'text' field."""
        data = {
            'text': 123,  # Should be a string
            'value': 123
        }
        serializer = CustomValueSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn('text', serializer.errors)

    def test_invalid_value_type(self):
        """Test case for invalid type of 'value' field."""
        data = {
            'text': 'Test Text',
            'value': 'Invalid Value'  # Should be a float
        }
        serializer = CustomValueSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn('value', serializer.errors)

    def test_custom_value_to_representation(self):
        """Test to_representation method for CustomValue."""
        custom_value = CustomValue(text='Test Text', value=123)
        serializer = CustomValueSerializer(instance=custom_value)
        expected_representation = {
            'text': 'Test Text',
            'value': 123
        }
        self.assertEqual(serializer.data, expected_representation)


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class TestListDataListsSerializer(TestCase):
    """Test ListDataList serializer"""
    def test_valid_list_data_lists_serializer(self):
        """Test serialiser with valid parameters."""
        data = {
            'data': [
                {'values': ['Value 1', 'Value 2'],
                 'names': ['Name 1', 'Name 2']},
                {'values': ['Value 3', 'Value 4'],
                 'names': ['Name 3', 'Name 4']}
            ]
        }
        serializer = ListDataListsSerializer(data=data)
        self.assertTrue(serializer.is_valid())

    def test_missing_data_field(self):
        """Test case for missing 'data' field."""
        data = {}
        serializer = ListDataListsSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn('data', serializer.errors)

    def test_empty_data_field(self):
        """Test case for empty 'data' field."""
        data = {'data': []}
        serializer = ListDataListsSerializer(data=data)
        self.assertTrue(serializer.is_valid())

    def test_invalid_data_type(self):
        """Test case for invalid type of 'data' field."""
        data = {'data': 'Invalid Data Type'}
        serializer = ListDataListsSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertIn('data', serializer.errors)

    def test_missing_values_field(self):
        """Test case for missing 'values' field inside 'data' list."""
        data = {'data': [{'names': ['Name 1', 'Name 2']},
                         {'values': ['Value 3',]}]}
        serializer = ListDataListsSerializer(data=data)
        self.assertFalse(serializer.is_valid())

    def test_missing_names_field(self):
        """Test case for missing 'names' field inside 'data' list."""
        data = {'data': [{'values': ['Value 1', 'Value 2']},
                         {'values': ['Value 3', 'Value 4']}]}
        serializer = ListDataListsSerializer(data=data)
        self.assertFalse(serializer.is_valid())

    def test_list_data_lists_to_representation(self):
        """Test to_representation method for ListDataLists."""
        single_data_list_1 = SingleDataList(
            values=['Value 1', 'Value 2', 'Value 3'],
            names=['Name 1', 'Name 2', 'Name 3']
        )
        single_data_list_2 = SingleDataList(
            values=['Value 4', 'Value 5', 'Value 6'],
            names=['Name 4', 'Name 5', 'Name 6']
        )
        list_data_lists = ListDataLists(data=[single_data_list_1,
                                              single_data_list_2])
        serializer = ListDataListsSerializer(instance=list_data_lists)
        expected_representation = {
            'data': [
                {'values': ['Value 1', 'Value 2', 'Value 3'],
                 'names': ['Name 1', 'Name 2', 'Name 3']},
                {'values': ['Value 4', 'Value 5', 'Value 6'],
                 'names': ['Name 4', 'Name 5', 'Name 6']}
            ]
        }
        self.assertEqual(serializer.data, expected_representation)

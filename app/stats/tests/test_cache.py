"""
Tests caching
"""
from django.test import TestCase
from dataclasses import dataclass
from stats.caching import CacheDecoratorFactory


@dataclass
class TrashContainer:
    """Class container of data"""
    data1: list
    data2: str
    data3: float


class TrashCreatorClass:
    """Class that have method that returns container object"""
    def __init__(self):
        self.counter = 0

    @CacheDecoratorFactory.create_decorator(timeout=1)
    def create_container(self):
        # Increment the last value of data1 every time it's called
        self.counter += 1
        return TrashContainer(data1=[1 + self.counter, 2, 3], data2="text",
                              data3=3.14)


class CacheTesting(TestCase):
    # The main idea is to create data with small TTL to easily recover
    # previous caching state
    def test_custom_class_caching(self):
        # Create an instance of TrashCreatorClass
        trash_creator = TrashCreatorClass()

        # Call the create_container method twice
        first_result = trash_creator.create_container()
        second_result = trash_creator.create_container()

        # Assert that the two calls return the same result due to caching
        self.assertEqual(first_result, second_result)

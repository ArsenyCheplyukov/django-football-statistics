"""
Tests for the match database api
"""
from django.test import TestCase, override_settings
from stats.models import (
    Country,
    League,
    Match,
    Player,
    PlayerAttributes,
    Team,
    TeamAttributes,
)


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class CountryTesting(TestCase):
    """Test country table in database"""

    def test_single_model_creation(self) -> None:
        """Test is country creation"""
        # create 'imaginary' country
        my_model = Country.objects.create(name='Atlantis')
        # compare the name of the model
        self.assertEqual(my_model.name, 'Atlantis')

    def test_multiple_models_creation(self) -> None:
        """Test multiple models creation"""
        # create 2 'imaginary' countries
        Country.objects.create(name='Atlantis')
        Country.objects.create(name='Averna')
        # get number of models
        models_amount = Country.objects.all()
        # compare the name of the model
        self.assertEqual(models_amount.count(), 2)


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class LeagueTesting(TestCase):
    """Test league table in database"""
    def test_single_model_creation(self) -> None:
        """Test single model creation"""
        # Create a country
        new_country = Country.objects.create(name='Atlantis')
        # Create a league associated with the country
        new_league = League.objects.create(name='Gridiron Gods',
                                           country=new_country)
        # Retrieve the league from the database
        saved_league = League.objects.get(pk=new_league.pk)
        # Check if the league was saved correctly
        self.assertEqual(saved_league.name, 'Gridiron Gods')
        self.assertEqual(saved_league.country.name, new_country.name)

    def test_multiple_models_creation(self) -> None:
        """Test multiple models creation"""
        # Create two countries
        first_country = Country.objects.create(name='Atlantis')
        second_country = Country.objects.create(name='Averna')
        # Create a league for each country
        League.objects.create(name='Gridiron Gods',
                              country=first_country)
        League.objects.create(name='End Zone Elite',
                              country=second_country)
        # Retrieve all leagues from the database
        all_leagues = League.objects.all()
        # Check if the correct number of leagues was created
        self.assertEqual(all_leagues.count(), 2)


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class PlayerTesting(TestCase):
    """Test country table in database"""

    def test_single_model_creation(self) -> None:
        """Test is country creation"""
        # create 'imaginary' country
        my_model = Player.objects.create(player_api_id=0,
                                         player_name='Player One',
                                         player_fifa_api_id=0,
                                         birthday='01/01/2000',
                                         height='200',
                                         weight='80')
        # compare the name of the model
        self.assertEqual(my_model.player_name, 'Player One')

    def test_multiple_models_creation(self) -> None:
        """Test multiple models creation"""
        # create 2 players
        Player.objects.create(player_api_id=0,
                              player_name='Player One',
                              player_fifa_api_id=0,
                              birthday='01/01/2000',
                              height='200',
                              weight='80')
        Player.objects.create(player_api_id=1,
                              player_name='Player Two',
                              player_fifa_api_id=1,
                              birthday='02/02/2000',
                              height='178',
                              weight='68')
        # get number of models
        models_amount = Player.objects.all()
        # compare the name of the model
        self.assertEqual(models_amount.count(), 2)


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class PlayerAttributesTesting(TestCase):
    """Test player attributes table in database"""

    def test_single_model_creation(self) -> None:
        """Test single model creation"""
        # Create a player
        player = Player.objects.create(player_api_id=0,
                                       player_name='Player One',
                                       player_fifa_api_id=0,
                                       birthday='01/01/2000',
                                       height='200',
                                       weight='80')
        # Create a player attributes instance associated with the player
        my_model = PlayerAttributes.objects.create(
            player_fifa_api=player,
            date='03/02/2024',
            overall_rating=90,
            potential=90)
        # Check if the player attributes instance was saved correctly
        self.assertEqual(my_model.date, '03/02/2024')

    def test_multiple_models_creation(self) -> None:
        """Test multiple models creation"""
        # Create two players
        player_1 = Player.objects.create(player_api_id=0,
                                         player_name='Player One',
                                         player_fifa_api_id=0,
                                         birthday='01/01/2000',
                                         height='200',
                                         weight='80')
        player_2 = Player.objects.create(player_api_id=1,
                                         player_name='Player Two',
                                         player_fifa_api_id=1,
                                         birthday='02/02/2000',
                                         height='178',
                                         weight='68')
        # Create player attributes instances associated with the players
        PlayerAttributes.objects.create(
            player_fifa_api=player_1,
            date='03/02/2024',
            overall_rating=90,
            potential=90)
        PlayerAttributes.objects.create(
            player_fifa_api=player_2,
            date='03/02/2024',
            overall_rating=90,
            potential=90)
        # Check if the correct number of player attributes instances created
        self.assertEqual(PlayerAttributes.objects.count(), 2)


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class TeamTesting(TestCase):
    """Test country table in database"""

    def test_single_model_creation(self) -> None:
        """Test is country creation"""
        # create 'imaginary' country
        my_model = Team.objects.create(team_api_id=0,
                                       team_fifa_api_id=0,
                                       team_long_name='Atlanta Hawks',
                                       team_short_name='AH')
        # compare the name of the model
        self.assertEqual(my_model.team_long_name, 'Atlanta Hawks')

    def test_multiple_models_creation(self) -> None:
        """Test multiple models creation"""
        # create 2 'imaginary' countries
        Team.objects.create(team_api_id=0,
                            team_fifa_api_id=0,
                            team_long_name='Atlanta Hawks',
                            team_short_name='AH')
        Team.objects.create(team_api_id=1,
                            team_fifa_api_id=1,
                            team_long_name='Bedrock Boulders',
                            team_short_name='BB')
        # get number of models
        models_amount = Team.objects.all()
        # compare the name of the model
        self.assertEqual(models_amount.count(), 2)


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class TeamAttributesTesting(TestCase):
    """Test team attributes table in database"""

    def test_single_model_creation(self) -> None:
        """Test single model creation"""
        # Create a team
        team = Team.objects.create(team_api_id=0,
                                   team_fifa_api_id=0,
                                   team_long_name='Atlanta Hawks',
                                   team_short_name='AH')
        # Create a team attributes instance associated with the team
        my_model = TeamAttributes.objects.create(
            team_fifa_api=team,
            team_api_id=team.team_api_id,
            date='02/02/2023'
        )
        # Check if the team attributes instance was saved correctly
        self.assertEqual(my_model.team_fifa_api, team)
        self.assertEqual(my_model.team_api_id, team.team_api_id)
        self.assertEqual(my_model.date, '02/02/2023')

    def test_multiple_models_creation(self) -> None:
        """Test multiple models creation"""
        # Create a team
        team = Team.objects.create(team_api_id=0,
                                   team_fifa_api_id=0,
                                   team_long_name='Atlanta Hawks',
                                   team_short_name='AH')
        # Create team attributes instances associated with the team
        TeamAttributes.objects.create(
            team_fifa_api=team,
            team_api_id=team.team_api_id,
            date='01/01/2024'
        )
        TeamAttributes.objects.create(
            team_fifa_api=team,
            team_api_id=team.team_api_id,
            date='02/02/2024'
        )
        # Check if the correct number of team attributes instances were created
        self.assertEqual(TeamAttributes.objects.all().count(), 2)


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class MatchTesting(TestCase):
    """Test match table in database"""

    def test_single_model_creation(self) -> None:
        """Test single model creation"""
        # Create country and league
        country = Country.objects.create(name='Atlantis')
        league = League.objects.create(name='Gridiron Gods', country=country)
        # Create two teams
        team_1 = Team.objects.create(team_api_id=0, team_fifa_api_id=0,
                                     team_long_name='Atlanta Hawks',
                                     team_short_name='AH')
        team_2 = Team.objects.create(team_api_id=1, team_fifa_api_id=1,
                                     team_long_name='Bay City Bluebirds',
                                     team_short_name='BCB')
        # Create a match associated with country, league, and teams
        my_model = Match.objects.create(
            country=country,
            league=league,
            season='Winter',
            stage=36,
            date='02/02/2024',
            match_api_id=0,
            home_team_api=team_1,
            away_team_api=team_2,
            home_team_goal=3,
            away_team_goal=2
        )
        # Check if the match was saved correctly
        self.assertEqual(my_model.country, country)
        self.assertEqual(my_model.league, league)
        self.assertEqual(my_model.season, 'Winter')
        self.assertEqual(my_model.stage, 36)
        self.assertEqual(my_model.date, '02/02/2024')
        self.assertEqual(my_model.match_api_id, 0)
        self.assertEqual(my_model.home_team_api, team_1)
        self.assertEqual(my_model.away_team_api, team_2)
        self.assertEqual(my_model.home_team_goal, 3)
        self.assertEqual(my_model.away_team_goal, 2)

    def test_multiple_models_creation(self) -> None:
        """Test multiple models creation"""
        # Create two unique countries and leagues
        country_1 = Country.objects.create(name='Atlantis')
        league_1 = League.objects.create(name='Gridiron Gods',
                                         country=country_1)
        country_2 = Country.objects.create(name='Olympus')
        league_2 = League.objects.create(name='Arena Warriors',
                                         country=country_2)
        # Create four unique teams
        team_1 = Team.objects.create(team_api_id=0, team_fifa_api_id=0,
                                     team_long_name='Atlanta Hawks',
                                     team_short_name='AH')
        team_2 = Team.objects.create(team_api_id=1, team_fifa_api_id=1,
                                     team_long_name='Bay City Bluebirds',
                                     team_short_name='BCB')
        team_3 = Team.objects.create(team_api_id=2, team_fifa_api_id=2,
                                     team_long_name='Bedrock Boulders',
                                     team_short_name='BB')
        team_4 = Team.objects.create(team_api_id=3, team_fifa_api_id=3,
                                     team_long_name='Coast City Angels',
                                     team_short_name='CCA')
        # Create two matches associated with country, league, and teams
        Match.objects.create(
            country=country_1,
            league=league_1,
            season='Winter',
            stage=36,
            date='02/02/2024',
            match_api_id=0,
            home_team_api=team_1,
            away_team_api=team_2,
            home_team_goal=3,
            away_team_goal=2
        )
        Match.objects.create(
            country=country_2,
            league=league_2,
            season='Winter',
            stage=36,
            date='02/02/2024',
            match_api_id=1,
            home_team_api=team_3,
            away_team_api=team_4,
            home_team_goal=1,
            away_team_goal=4
        )
        # Check if the correct number of matches were created
        self.assertEqual(Match.objects.count(), 2)

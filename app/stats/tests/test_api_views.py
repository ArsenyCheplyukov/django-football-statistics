from django.test import TestCase, override_settings
from rest_framework import status
from django.urls import reverse

from stats.models import (
    Country,
    Team,
    League,
    Player,
    PlayerAttributes,
    Match
)


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class EnvironmentForTest(TestCase):
    def setUp(self):
        self.country = Country.objects.create(name='Test Country')
        Country.objects.create(name='Test Country 2')
        self.league = League.objects.create(name='Test League',
                                            country=self.country)
        self.team = Team.objects.create(team_api_id=0,
                                        team_fifa_api_id=0,
                                        team_long_name='Test Team',
                                        team_short_name='TT')
        self.player = Player.objects.create(player_api_id=0,
                                            player_name='Test Player',
                                            player_fifa_api_id=0,
                                            birthday='01/01/1990')
        player2 = Player.objects.create(player_api_id=1,
                                        player_name='Test Player 2',
                                        player_fifa_api_id=1,
                                        birthday='02/02/1990')
        self.match = Match.objects.create(country=self.country,
                                          league=self.league,
                                          season='winter',
                                          stage=36,
                                          date='01/01/2010',
                                          match_api_id=0,
                                          home_team_api=self.team,
                                          away_team_api=self.team,
                                          home_team_goal=2,
                                          away_team_goal=2)

        self.player_attributes = PlayerAttributes.objects.create(
            player_fifa_api=self.player,
            player_api=self.player,
            date='01/01/2010',
            potential=70
        )
        PlayerAttributes.objects.create(
            player_fifa_api=self.player,
            player_api=self.player,
            date='01/01/2011',
            potential=75
        )
        PlayerAttributes.objects.create(
            player_fifa_api=player2,
            player_api=player2,
            date='01/01/2011',
            potential=75
        )
        # name, object ref, is query for list, is query for home
        self.grouped_parameters = {
            'country': (self.country, True, False),
            'league': (self.league, True, True),
            'team': (self.team, True, True),
            'player': (self.player, True, False),
            'match': (self.match, True, False),
        }


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class TestListView(EnvironmentForTest):
    def test_valid_request(self):
        for key in self.grouped_parameters.keys():
            url = reverse('stats:api_list_view', kwargs={'name': key})
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_name(self):
        url = reverse('stats:api_list_view', kwargs={'name': 'invalid_name'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class TestQueryListView(EnvironmentForTest):
    def test_valid_request(self):
        for key, value in self.grouped_parameters.items():
            if value[1]:
                url = reverse('stats:api_list_query_view',
                              kwargs={'name': key, 'query_id': 0})
                response = self.client.get(url)
                self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_name(self):
        url = reverse('stats:api_list_query_view',
                      kwargs={'name': 'invalid_name', 'query_id': 0})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_query_id(self):
        for key in self.grouped_parameters.keys():
            url = reverse('stats:api_list_query_view',
                          kwargs={'name': key, 'query_id': 1000})
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class TestHomeView(EnvironmentForTest):
    def test_valid_request(self):
        for key, value in self.grouped_parameters.items():
            url = reverse('stats:api_home_view',
                          kwargs={'name': key, 'id': value[0].id})
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_name(self):
        url = reverse('stats:api_home_view',
                      kwargs={'name': 'invalid_name', 'id': 0})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_id(self):
        for key in self.grouped_parameters.keys():
            url = reverse('stats:api_home_view',
                          kwargs={'name': key, 'id': 1000})
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class TestQueryHomeView(EnvironmentForTest):
    def test_valid_request(self):
        for key, value in self.grouped_parameters.items():
            if value[2]:
                url = reverse('stats:api_home_query_view',
                              kwargs={'name': key, 'id': value[0].id,
                                      'query_id': 0})
                response = self.client.get(url)
                self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_name(self):
        url = reverse('stats:api_home_query_view',
                      kwargs={'name': 'invalid_name', 'id': 0, 'query_id': 0})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_id(self):
        for key in self.grouped_parameters.keys():
            url = reverse('stats:api_home_query_view',
                          kwargs={'name': key, 'id': 1000, 'query_id': 0})
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_query_id(self):
        for key, value in self.grouped_parameters.items():
            if value[2]:
                url = reverse('stats:api_home_query_view',
                              kwargs={'name': key, 'id': value[0].id,
                                      'query_id': 1000})
                response = self.client.get(url)
                self.assertEqual(response.status_code,
                                 status.HTTP_404_NOT_FOUND)


@override_settings(
    CACHES={'default': {'BACKEND':
                        'django.core.cache.backends.dummy.DummyCache'}}
    )
class TestDjangoViews(EnvironmentForTest):
    def test_valid_list_view(self):
        """Test valid link with existing statistics name."""
        # Iterate through all possible names
        for name in ['country', 'league', 'team', 'match']:  # 'player',
            url = reverse('stats:list_view', kwargs={'name': name})
            response_valid = self.client.get(url)
            self.assertEqual(response_valid.status_code, 200)

    def test_invalid_list_view(self):
        """Test invalid link with non-existent statistics name."""
        url = reverse('stats:list_view', kwargs={'name': 'qwerty'})
        response_invalid = self.client.get(url)
        self.assertEqual(response_invalid.status_code, 404)

    def test_valid_concrete_view(self):
        # Iterate through all possible names
        for name in ['country', 'league', 'team', 'player', 'match']:
            # Test valid link with existing name and id
            valid_attribute = getattr(self, name)
            url = reverse('stats:home_view', kwargs={'name': name,
                                                     'id': valid_attribute.id})
            response_valid = self.client.get(url)
            self.assertEqual(response_valid.status_code, 200)

    def test_invalid_id_concrete_view(self):
        # Test invalid link with non-existent name and id
        for name in ['country', 'league', 'team', 'player', 'match']:
            url = reverse('stats:home_view', kwargs={'name': name,
                                                     'id': 999})
            response_invalid = self.client.get(url)
            self.assertEqual(response_invalid.status_code, 404)

    def test_invalid_concrete_view_name(self):
        # Test invalid link with non-existent name and id
        url = reverse('stats:home_view', kwargs={'name': 'qwerty', 'id': 999})
        response_invalid = self.client.get(url)
        self.assertEqual(response_invalid.status_code, 404)

    def test_pagination_granulation_in_list_view(self):
        """Test pagination in list view."""
        # Iterate through all possible names
        for name in ['country', 'league', 'team', 'player', 'match']:
            # Get the URL for the list view
            url = reverse('stats:list_view', kwargs={'name': name})
            # Make a GET request to the list view URL
            response = self.client.get(url)
            # Assert that the response status code is 200
            self.assertEqual(response.status_code, 200)
            # Check if pagination is present in the response context
            self.assertIn('paginator', response.context)
            # Get the paginated queryset from the response context
            paginator = response.context['paginator']
            # Assert that the paginator is an instance of Paginator
            self.assertEqual(paginator['current_page'], 0)
            self.assertEqual(paginator['total_pages'], 1)
            self.assertEqual(paginator['page_range'], [0])
            self.assertEqual(paginator['has_next'], False)
            self.assertEqual(paginator['has_previous'], False)
            # Assert that the paginated queryset contains data
            self.assertTrue('data' in paginator)

    def test_valid_pagination_page(self):
        """Test valid pagination page."""
        # Iterate through all possible names
        for name in ['country', 'league', 'team', 'player', 'match']:
            # Get the URL for the list view with pagination page 1
            url = reverse('stats:list_view', kwargs={'name': name}) + '?page=0'

            # Make a GET request to the list view URL
            response = self.client.get(url)

            # Assert that the response status code is 200
            self.assertEqual(response.status_code, 200)

            # Check if pagination is present in the response context
            self.assertIn('paginator', response.context)

            # Get the paginated queryset from the response context
            paginator = response.context['paginator']

            # Assert that the paginator is an instance of Paginator
            self.assertEqual(paginator['current_page'], 0)
            self.assertEqual(paginator['total_pages'], 1)
            self.assertEqual(paginator['page_range'], [0])
            self.assertEqual(paginator['has_next'], False)
            self.assertEqual(paginator['has_previous'], False)

    def test_invalid_pagination_page(self):
        """Test invalid pagination page."""
        # Iterate through all possible names
        for name in ['country', 'league', 'team', 'player', 'match']:
            # Get the URL for the list view with a large invalid page number
            url = reverse('stats:list_view', kwargs={'name': name}) + \
                '?page=1000000'

            # Make a GET request to the list view URL
            response = self.client.get(url)

            # Assert that the response status code is 200
            self.assertEqual(response.status_code, 200)

            # Check if the first page is shown
            self.assertIn('0</a>', response.content.decode())

    def test_wrong_type_pagination_page(self):
        """Test wrong type pagination page."""
        # Iterate through all possible names
        for name in ['country', 'league', 'team', 'player', 'match']:
            # Get the URL for the list view with invalid type for page number
            url = reverse('stats:list_view', kwargs={'name': name}) + \
                '?page=asd'

            # Make a GET request to the list view URL
            response = self.client.get(url)

            # Assert that the response status code is 404
            self.assertEqual(response.status_code, 404)

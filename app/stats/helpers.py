"""
Provide som functions that may be used in multiple modules
"""
from typing import List

from django.db.models import Model

import re

from .models import Match


FIELDS_TO_EXCLUDE = ['teamattributes', 'playerattributes']
EXCLUDE_FOR_MATCH = ['b365h', 'b365d', 'b365a', 'goal', 'shoton',
                     'shotoff', 'foulcommit', 'card', 'cross',
                     'corner', 'possession']


def get_model_field_names(model: Model) -> List[str]:
    """Get all field names of a Django model excluding specified fields."""
    if model is Match:
        excluded_regex = re.compile(
            r'^(home_player|away_player)_[a-zA-Z]*\d+[a-zA-Z]*$'
        )
        length_three_regex = re.compile(r'^\w{3}$')

        return [
            field.name for field in model._meta.get_fields()
            if not field.is_relation
            and field.name not in EXCLUDE_FOR_MATCH
            and not excluded_regex.match(field.name)
            and not length_three_regex.match(field.name)
        ]
    else:
        return [
            field.name for field in model._meta.get_fields()
            if not field.is_relation and field.name not in FIELDS_TO_EXCLUDE
        ]


def prepare_names(names_list: List[str]) -> List[str]:
    return [name.replace('_', ' ').capitalize() for name in names_list]

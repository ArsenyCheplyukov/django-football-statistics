"""
Statistics URLs for the website
"""
from django.urls import path, include

from stats import views


app_name = 'stats'


# Backend URLs
backend_patterns = [
    path('list/<str:name>/', views.ListView.as_view(),
         name='api_list_view'),
    path('list/<str:name>/queries/<int:query_id>/',
         views.QueryListView.as_view(), name='api_list_query_view'),
    path('home/<str:name>/<int:id>/', views.HomeView.as_view(),
         name='api_home_view'),
    path('home/<str:name>/<int:id>/queries/<int:query_id>/',
         views.QueryHomeView.as_view(), name='api_home_query_view')
]

# Vanilla Django URLs
vanilla_patterns = [
    path('list/<str:name>/', views.list_view, name='list_view'),
    path('home/<str:name>/<int:id>/', views.concrete_view, name='home_view'),
    path('', views.home, name='home'),
]


urlpatterns = [
    # Backend URLs with '/api/' prefix
    path('api/', include(backend_patterns)),

    # Vanilla Django URLs without any prefix
    path('', include(vanilla_patterns)),
]

from stats.queries import (ListCountryStatistics,
                           ConcreteLeagueStatistics)
from stats.models import (Country,
                          League,
                          Team,
                          Player)
# from dataclass import dataclass
from typing import List
from json import dumps
from random import choice, choices, sample, randint


# @dataclass
# class OutputMatchData:
#     country: Country
#     league: League
#     home_team: Team
#     away_team: Team
#     home_goal: int
#     away_goal: int
#     home_players: List[Player]
#     away_players: List[Player]


def generate_random_date(start_year: int = 1990, end_year: int = 2024):
    """Generate random date in format of: YYYY-MM-DD"""
    # Generate a random year
    year = randint(start_year, end_year)

    # Generate a random month
    month = randint(1, 12)

    # Generate a random day
    if month in [1, 3, 5, 7, 8, 10, 12]:
        day = randint(1, 31)
    elif month in [4, 6, 9, 11]:
        day = randint(1, 30)
    else:
        # Check for leap year
        if year % 4 == 0 and (year % 100 != 0 or year % 400 == 0):
            day = randint(1, 29)
        else:
            day = randint(1, 28)

    # Format the date
    date_str = f"{year}-{month:02d}-{day:02d}"

    return date_str


def generate_random_stage() -> int:
    """Generate a random stage value for a random match.
       Returns the randomly generated stage value.
    """
    # Define probabilities for each type of competition
    competition_probabilities = {
        'league': 0.6,
        'knockout': 0.3,
        'qualification': 0.1
    }

    # Define the stage ranges for each type of competition
    competition_stages = {
        'league': (1, 38),  # First and last stage of a league
        'knockout': (1, 7),  # First and last stage of a knockout tournament
        'qualification': (1, 3)  # First and last stage of qualification rounds
    }

    # Weighted selection of competition type based on probabilities
    competition_type = choices(
        list(competition_probabilities.keys()),
        weights=list(competition_probabilities.values())
    )[0]

    # Get the stage range for the selected competition type
    min_stage, max_stage = competition_stages[competition_type]

    # Generate a random stage within the selected range
    random_stage = randint(min_stage, max_stage)

    return random_stage


def decomposition(sum_of_values: int, size: int) -> List[int]:
    """Split values into list of size with const sum_of_values."""
    result = [0 for _ in range(size)]
    for _ in range(sum_of_values):
        result[choice(range(size))] += 1
    return result


def generate_random_match_data():
    """Create json file with needed match data."""
    # get all countries
    country_list = list(Country.objects
                        .values_list('id', flat=True)
                        .distinct())
    # get random country name
    country_id = choice(country_list)
    # and get country refference
    country = Country.objects.get(id=country_id)
    # then get list of leagues for that country
    leagues_list = League.objects.filter(country=country)
    # get random league from that list
    league = choice(leagues_list)
    league_id = league.id
    # get distribution of teams for calculating probabilities
    top_teams_graphs = ConcreteLeagueStatistics(league_id) \
        .top_teams_by_points()
    # extract x_data and y_data for each team
    top_teams_data = list(zip(top_teams_graphs.x_data,
                              top_teams_graphs.y_data))
    # get 2 particular teams
    two_teams = sample(top_teams_data, 2)
    # parse data into frequencies and names for this teams
    frequencies = [two_teams[i][1] for i in range(len(two_teams))]
    names = [two_teams[i][0] for i in range(len(two_teams))]
    # calculate probabilities
    probabilities = [frequencies[i]/sum(frequencies) for i in
                     range(len(frequencies))]
    # generate win team
    win_team_name = choices(names, weights=probabilities)[0]
    # generate win team goals
    win_team_goal = choice(range(1, 11))
    # generate precentage of losing teams to wining team to get goals amount in future
    relativity = float(choice(range(10)))/10
    lose_team_goal = relativity * win_team_goal

    # The main problem there, is that there are huge amount of empty
    # info for players. That means that there is not enougt data to
    # make distribution with probabilities

    # the solution is quite strange, so I use all players and get fixed amount
    # of them (there are no field for player hanging for some concrete team)
    all_players = set(Player.objects.values_list('id', flat=True).all())
    home_team_players = sample(list(all_players), 11)
    all_players -= set(home_team_players)
    away_team_players = sample(list(all_players), 11)
    # distribute goals across them
    home_goals = decomposition(win_team_goal, 11)
    away_goals = decomposition(win_team_goal, 11)
    # generate output match data as json
    home_players = {f"home_player_{i+1}_id": home_team_players[i] for i in
                    range(11)}
    away_players = {f"away_player_{i+1}_id": away_team_players[i] for i in
                    range(11)}
    home_players_goal = {f"home_player_{i+1}_goals": home_goals[i] for i in
                         range(11)}
    away_players_goal = {f"away_player_{i+1}_goals": away_goals[i] for i in
                         range(11)}
    date = generate_random_date()
    data = {
        "country_id": country_id,
        "league_id": league_id,
        "season": f"{date[:4]}/{str(int(date[:4])+1)}",
        "stage": generate_random_stage(),
        "home_team_name": names[0],
        "away_team_name": names[1],
        "home_team_goal": (win_team_goal if names[0] == win_team_name else
                           lose_team_goal),
        "away_team_goal": (win_team_goal if names[1] == win_team_name else
                           lose_team_goal),
        **home_players,
        **away_players,
        **home_players_goal,
        **away_players_goal
    }
    return dumps(data)

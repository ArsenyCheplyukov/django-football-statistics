"""
Provide generic way to organize serializers
"""
from rest_framework import serializers


class ListOnlyField(serializers.ListField):
    """Allows only list data"""
    def to_internal_value(self, data):
        if not isinstance(data, list):
            raise serializers.ValidationError("Expected a list.")
        return super().to_internal_value(data)


class CharOnlyField(serializers.CharField):
    """Class that expects only string values"""
    def to_internal_value(self, data):
        if not isinstance(data, str):
            raise serializers.ValidationError("Expected a string.")
        return super().to_internal_value(data)


class GraphSerializer(serializers.Serializer):
    name = CharOnlyField(required=True)
    x_axis = CharOnlyField(required=True)
    y_axis = CharOnlyField(required=True)
    x_data = ListOnlyField(child=serializers.CharField(), required=True)
    y_data = ListOnlyField(child=serializers.FloatField(), required=True)
    legends = ListOnlyField(child=serializers.CharField(),
                            required=False)

    def validate_data(self, data):
        for item in data:
            if len(item.get('x_data')) != len(item.get('y_data')):
                raise serializers.ValidationError("Length of 'values' and"
                                                  "'names' must be the same.")
        return data

    def to_representation(self, instance):
        return {
            'name': instance.name,
            'x_axis': instance.x_axis,
            'y_axis': instance.y_axis,
            'x_data': instance.x_data,
            'y_data': instance.y_data,
            'legends': instance.legends if hasattr(
                instance, 'legends') else []
        }


class SingleDataListSerializer(serializers.Serializer):
    values = ListOnlyField(child=serializers.CharField(),
                           required=True)
    names = ListOnlyField(child=serializers.CharField(),
                          required=True)

    def validate(self, attrs):
        values = attrs.get('values', [])
        names = attrs.get('names', [])

        if len(values) != len(names):
            raise serializers.ValidationError("Length of 'values' and 'names'"
                                              "must be the same.")

        return attrs


class CustomValueSerializer(serializers.Serializer):
    text = CharOnlyField(required=True)
    value = serializers.FloatField(required=True)


class ListDataListsSerializer(serializers.Serializer):
    data = ListOnlyField(child=SingleDataListSerializer(),
                         required=True)

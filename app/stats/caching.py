"""
Implement all that connected with caching
"""
from typing import Callable, Any, Protocol
from functools import wraps
import redis
from django.core.cache import caches, cache
from app import settings
if settings.caching_approach not in ['redis', 'django']:
    from uwsgidecorators import timer


class RedisManager:
    def __init__(self, host='redis', port=6379, db=1,
                 username='default', password='secret'):
        self.host = host
        self.port = port
        self.db = db
        self.username = username
        self.password = password

    def __enter__(self):
        # Connect to Redis using the hiredis parser
        self.client = redis.Redis(host=self.host, port=self.port, db=self.db,
                                  username=self.username,
                                  password=self.password,
                                  decode_responses=True)
        return self.client

    def __exit__(self, exc_type, exc_value, traceback):
        # Close the connection to Redis
        if self.client is not None:
            self.client.close()


class CacheStrategy(Protocol):
    """Abstract base class for caching strategies."""
    def cache_get(self, key: str) -> Any:
        """Retrieve data from cache."""
        pass

    def cache_set(self, key: str, value: Any, timeout: int) -> None:
        """Store data in cache."""
        pass


class BaseCacheDecorator(Protocol):
    """Base class for caching decorators."""
    def __init__(self, *args, **kwargs) -> None:
        pass

    def __call__(self, func: Callable[..., Any]) -> Callable[..., Any]:
        """Callable method to serve as the decorator."""
        pass

    def _generate_cache_key(self, func: Callable[..., Any], args: Any,
                            kwargs: Any) -> str:
        """Generate cache key."""
        class_name = func.__qualname__.split('.')[0] if args else ''
        method_name = func.__name__
        return f"{class_name}.{method_name}"


class OneStageCacheDecorator(BaseCacheDecorator):
    """One particular class decorator for caching."""
    def __init__(self, strategy: CacheStrategy, timeout: int = 3600) -> None:
        self.strategy = strategy
        self.timeout = timeout

    def __call__(self, func: Callable[..., Any]) -> Callable[..., Any]:
        """Handle caching on one step after database query."""
        @wraps(func)
        def wrapper(*args: Any, **kwargs: Any) -> Any:
            cache_key = self._generate_cache_key(func, args, kwargs)
            cached_result = self.strategy.cache_get(cache_key) or None
            if cached_result is not None:
                print("Use cached data")
                return cached_result
            else:
                print("Set cache")
                result = func(*args, **kwargs)
                self.strategy.cache_set(cache_key, result, self.timeout)
                return result
        return wrapper


def background_cache_update(step_alias: str, key: str, data: Any,
                            timeout: int = 3600) -> None:
    """Perform background update from given step to started app instance"""
    # if found data in global cache - update global
    if step_alias == "cache":
        caches['default'].set(key, data, timeout)
    if step_alias == 'database':
        caches['default'].set(key, data, timeout)
        caches['global'].set(key, data, timeout)


class MultistageCacheDecorator(BaseCacheDecorator):
    """Two stage caching: global and local"""
    def __init__(self, timeout: int = 3600):
        self.timout = timeout

    def __call__(self, func: Callable[..., Any]) -> Callable[..., Any]:
        """Callable method to serve as the decorator."""
        @wraps(func)
        def wrapper(*args: Any, **kwargs: Any) -> Any:
            cache_key = self._generate_cache_key(func, args, kwargs)
            cached_result = caches['default'].get(cache_key) or None
            if cached_result is not None:
                print("Use local cached data")
                return cached_result
            else:
                result = caches['global'].get(cache_key) \
                    or None
                if cached_result is not None:
                    print("Use global cached data, update local one in "
                          "background")
                    timer(background_cache_update(step_alias="cache",
                                                  key=cache_key,
                                                  data=cached_result),
                          target='mule')
                    return cached_result
                else:
                    print("Get data from database")
                    result = func(*args, **kwargs)
                    print("Start caching in background")
                    timer(background_cache_update(step_alias="database",
                                                  key=cache_key,
                                                  data=result), target='mule')
                    return result
        return wrapper


class RedisDjangoCacheStrategy(CacheStrategy):
    """Caching strategy using Redis."""
    def cache_get(self, key: str) -> Any:
        """Retrieve data from Redis cache."""
        item = cache.get(key)
        return item

    def cache_set(self, key: str, value: Any, timeout: int) -> None:
        """Store data in Redis cache."""
        cache.set(key, value, timeout=timeout)


# class RedisCacheStrategy(CacheStrategy):
#     """Caching strategy using Redis."""
#     def __init__(self):
#         self.redis_client = redis.Redis(host="redis", port=6379, db=1,
#                                         username="default",
#                                         password="secret",
#                                         decode_responses=True)

#     def cache_get(self, key: str) -> Any:
#         """Retrieve data from Redis cache."""
#         # with RedisManager() as redis_client:
#         item = self.redis_client.get(key)
#         return item

#     def cache_set(self, key: str, value: Any, timeout: int) -> None:
#         """Store data in Redis cache."""
#         # with RedisManager() as redis_client:
#         state = self.redis_client.set(key, value, ex=timeout)
#         a = 2


class uWSGICacheStrategy(CacheStrategy):
    """Caching strategy using uWSGI caching framework."""
    def cache_get(self, key: str) -> Any:
        """Retrieve data from UWSGI cache."""
        item = cache.get(key)
        return item

    def cache_set(self, key: str, value: Any, timeout: int) -> None:
        """Store data in UWSGI cache."""
        cache.set(key, value, timeout=timeout)


class CacheDecoratorFactory:
    """Factory class for creating caching decorators."""
    @classmethod
    def create_decorator(cls,
                         strategy_type: str = None,
                         timeout: int = 3600) -> BaseCacheDecorator:
        """Create a caching decorator based on the provided strategy type."""
        if strategy_type is None:
            strategy_type = settings.caching_approach
        if strategy_type.lower() == 'redis':
            strategy = RedisDjangoCacheStrategy()
            return OneStageCacheDecorator(strategy=strategy, timeout=timeout)
        # elif strategy_type.lower() == 'redis':
        #     strategy = RedisCacheStrategy()
        elif strategy_type.lower() == 'uwsgi':
            strategy = uWSGICacheStrategy()
            return OneStageCacheDecorator(strategy=strategy, timeout=timeout)
        elif strategy_type.lower() == 'multi':
            return MultistageCacheDecorator(timeout=timeout)
        else:
            raise ValueError("Invalid strategy type. Supported types:"
                             "'redis', 'uwsgi'")

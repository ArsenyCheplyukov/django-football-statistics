"""
Classes that describe custom datatypes
"""
from numbers import Number
from typing import List, Any, Optional, Protocol

from enum import Enum
from dataclasses import dataclass
# provide automatic plot creating
import plotly.express as px
import pandas as pd

# helper functions
from .helpers import prepare_names, get_model_field_names


class PageParts(Enum):
    """Representation of generic page in fixed parts"""
    ENTRANCE = 1
    VISUAL = 2
    LIST = 3


class CustomDataType(Protocol):
    page_part: PageParts = None


class Graph(CustomDataType):
    page_part = PageParts.VISUAL

    def __init__(self, name: str, x_axis: str, y_axis: str,
                 x_data: List[Number], y_data: List[Number],
                 legends: Optional[List[str]] = None):
        self.name = name
        self.x_axis = x_axis
        self.y_axis = y_axis
        self.x_data = x_data
        self.y_data = y_data
        self.legends = legends if legends is not None else []
        self.plot = self.plot_graph()

    def plot_graph(self):
        self.df = pd.DataFrame({self.x_axis: self.x_data,
                                self.y_axis: self.y_data})
        fig = px.line(self.df,
                      x=self.x_axis,
                      y=self.y_axis,
                      title=self.name,
                      template='plotly_dark')
        fig.update_layout(
            title={'font': {'size': 16, 'family': 'Times New Roman'}},
            font={'size': 14, 'family': 'Times New Roman'},
            xaxis=dict(showgrid=True, gridwidth=1, gridcolor='LightGray',
                       tickangle=-45),
            yaxis=dict(showgrid=True, gridwidth=1, gridcolor='LightGray'),
        )
        return fig.to_html()


class Histogram(Graph):
    def plot_graph(self):
        self.df = pd.DataFrame({self.x_axis: self.x_data,
                                self.y_axis: self.y_data})
        fig = px.bar(
            self.df,
            x=self.x_axis,
            y=self.y_axis,
            title=self.name,
            template='plotly_dark',
        )
        fig.update_layout(
            title={'font': {'size': 16, 'family': 'Times New Roman'}},
            font={'size': 14, 'family': 'Times New Roman'},
            xaxis=dict(showgrid=False, tickangle=-45),
            yaxis=dict(showgrid=False),
        )
        return fig.to_html()


class SingleDataList(CustomDataType):
    # __slots__ = ["values", "page_part"]
    page_part = PageParts.LIST

    def __init__(self, values: List[Any], names: List[str]):
        self.values = values
        self.names = names

    def __iter__(self):
        return zip(self.names, self.values)

    @classmethod
    def create_from_queryset(cls, queryset, model):
        if queryset:
            names = get_model_field_names(model)
            values = list()
            for name in names:
                try:
                    values.append(queryset[name])
                except TypeError:
                    values.append(getattr(queryset, name))
                except KeyError:
                    pass

            # values = [getattr(queryset, name) for name in names
            #           if hasattr(queryset, name)]
            names = prepare_names(names)
            return cls(values=values, names=names)
        else:
            # Return an empty SingleDataList if no object is found
            return None  # Return None instead of an empty instance


class ListDataLists(CustomDataType):
    # __slots__ = ["data", "page_part"]
    page_part = PageParts.LIST

    def __init__(self, data: List[SingleDataList]):
        self.data = data

    def __len__(self):
        return len(self.data)

    def __iter__(self):
        return iter(self.data)

    @classmethod
    def create_from_queryset(cls, queryset, model):
        data_lists = []
        for obj in queryset:
            single_data_list = SingleDataList.create_from_queryset(
                queryset=obj, model=model
            )
            if single_data_list:
                data_lists.append(single_data_list)
        return cls(data=data_lists)


class CustomValue(CustomDataType):
    # __slots__ = ["text", "value", "page_part"]
    page_part = PageParts.ENTRANCE

    def __init__(self, text: str, value: Any):
        self.text = text
        self.value = value


@dataclass
class ListViewFields:
    """Class that represents fixed fields in list-views"""
    name: str
    entrance: List[CustomValue]
    visual: List[CustomDataType]
    queryset: ListDataLists


@dataclass
class SingleViewFields:
    name: str
    entrance: List[CustomValue]
    visual: List[CustomDataType]
    fields: SingleDataList

"""
Define structured queries for generating various statistics related to teams,
players, leagues, matches, countries
"""
# for definition of used types
from typing import List, Tuple, Optional, Type, Protocol

from django.db.models import (
    Model, QuerySet,
)
from django.db import connection

from .models import (
    Country,
    Player,
    Team,
    League,
    Match,
)

from .datatypes import (
    PageParts,
    CustomDataType,
    CustomValue,
    Graph,
    Histogram,
    SingleDataList,
    ListDataLists,
    ListViewFields,
    SingleViewFields
)
# helpers
from .helpers import get_model_field_names

from .caching import CacheDecoratorFactory


def queryset_to_x_y(queryset: QuerySet,
                    x_name: str,
                    y_name: str) -> Tuple[List, List]:
    """Convert a queryset to an x and y axis lists"""
    x, y = list(), list()
    for i in queryset:
        try:
            x.append(i[x_name])
            y.append(i[y_name])
        except KeyError:
            pass
    return (x, y)


def queryset_to_count(queryset: QuerySet, name: str) -> Tuple[List, List]:
    """Convert queryset in key of given name into ranking (place: value)"""
    y = list()
    for i in queryset:
        try:
            y.append(i[name])
        except KeyError:
            pass
    x = list(range(len(y)))
    return (x, y)


class BaseStatistics(Protocol):
    """Abstract statistics class"""
    model: Optional[Type[Model]] = None

    def display(self) -> List[CustomDataType]:
        """Show all related statistics"""


class ListBaseStatistics(BaseStatistics):
    """Abstract statistics class"""
    def display_list(self) -> ListDataLists:
        """Get all values of related model"""
        pass


class ConcreteBaseStatistics(BaseStatistics):
    """Abstract statistics for concrete class"""
    filtered_query: Optional[Type[Model]] = None

    def __init__(self, id: int) -> None:
        """Define id of concrete class"""
        self.id = id

    def get_current_object_fields(self) -> SingleDataList:
        """Get all fields for concrete statisftics"""
        pass


class ConcreteTeamStatistics(ConcreteBaseStatistics):
    """Statistics for concrete team"""
    def __init__(self, id: int):
        """Customize base class constructor behavior"""
        super().__init__(id)
        self.model = Team
        with connection.cursor() as cursor:
            cursor.execute("""
                        SELECT team_api_id
                        FROM team
                        WHERE id = %s;""", [id])
            row = cursor.fetchone()
            if row is not None:
                self.filtered_query_id = row[0]
            else:
                raise Team.DoesNotExist
        self.query = Team.objects.get(team_api_id=self.filtered_query_id)

    def get_current_object_fields(self) -> SingleDataList:
        """Get list of fields for current user"""
        super().get_current_object_fields()
        return SingleDataList.create_from_queryset(
            queryset=self.query,
            model=self.model
        )

    def display(self) -> List[CustomDataType]:
        """Show Team related statistics"""
        statistics = [
            self.total_matches_played,
            self.win_loss_draw_count,
            self.goals_scored_and_conceded,
            self.avg_goals_per_match
        ]
        return statistics

    def total_matches_played(self) -> CustomValue:
        """Calculate the total number of matches played by the given team"""
        with connection.cursor() as cursor:
            cursor.execute("""
                SELECT COUNT(*)
                FROM match
                WHERE home_team_api_id = %s OR away_team_api_id = %s;
            """, [self.filtered_query_id, self.filtered_query_id])
            total_matches = cursor.fetchone()[0]
        return CustomValue(
            text=f"The total amout of matches is: {total_matches or 0}",
            value=(total_matches or 0)
        )

    def win_loss_draw_count(self) -> CustomValue:
        """Calculate the count of wins, losses, and draws for the given team"""
        with connection.cursor() as cursor:
            cursor.execute("""
                SELECT
                    SUM(CASE
                            WHEN home_team_api_id = %s AND home_team_goal >
                                away_team_goal THEN 1
                            WHEN away_team_api_id = %s AND home_team_goal <
                                away_team_goal THEN 1
                            ELSE 0
                        END) AS win_count,
                    SUM(CASE
                            WHEN home_team_api_id = %s AND home_team_goal <
                                away_team_goal THEN 1
                            WHEN away_team_api_id = %s AND home_team_goal >
                                away_team_goal THEN 1
                            ELSE 0
                        END) AS loss_count,
                    SUM(CASE
                            WHEN home_team_goal = away_team_goal THEN 1
                            ELSE 0
                        END) AS draw_count
                FROM match
                WHERE home_team_api_id = %s OR away_team_api_id = %s;
            """, [self.filtered_query_id for _ in range(6)])
            result = cursor.fetchone()
            win_count = result[0] or 0
            loss_count = result[1] or 0
            draw_count = result[2] or 0
        return CustomValue(
            text=f"The win/loss/draw count is: {win_count}/{loss_count}/"
                 f"{draw_count}",
            value=(win_count, loss_count, draw_count)
        )

    def goals_scored_and_conceded(self) -> CustomValue:
        """Goals scored and conceded by given team from all time"""
        with connection.cursor() as cursor:
            cursor.execute("""
                SELECT
                    SUM(goals_scored) AS scored,
                    SUM(goals_conceded) AS conceded
                FROM (
                    SELECT
                        CASE
                            WHEN m.home_team_api_id = %s THEN m.home_team_goal
                            ELSE 0
                        END AS goals_scored,
                        CASE
                            WHEN m.away_team_api_id = %s THEN m.away_team_goal
                            ELSE 0
                        END AS goals_conceded
                    FROM match m
                    WHERE m.home_team_api_id = %s OR m.away_team_api_id = %s
                    UNION ALL
                    SELECT
                        CASE
                            WHEN m.away_team_api_id = %s THEN m.away_team_goal
                            ELSE 0
                        END AS goals_scored,
                        CASE
                            WHEN m.home_team_api_id = %s THEN m.home_team_goal
                            ELSE 0
                        END AS goals_conceded
                    FROM match m
                    WHERE m.away_team_api_id = %s OR m.home_team_api_id = %s
                ) AS subquery;
            """, [self.filtered_query_id for _ in range(8)])
            result = cursor.fetchone()
            scored = result[0] if result[0] is not None else 0
            conceded = result[1] if result[1] is not None else 0
        return CustomValue(
            text="The number of goals scored/conceded from all the time is: "
                 f"{scored}/{conceded}",
            value=(scored, conceded)
        )

    def avg_goals_per_match(self) -> CustomValue:
        """Calculate the average goals scored and conceded per match"""
        with connection.cursor() as cursor:
            cursor.execute("""
                SELECT
                    CAST(goals_scored AS FLOAT) / all_matches_count
                        AS scored,
                    CAST(goals_conceded AS FLOAT) / all_matches_count
                        AS conceded
                FROM (
                    SELECT
                        SUM(CASE
                            WHEN home_team_api_id = %s THEN home_team_goal
                            ELSE away_team_goal
                            END
                        ) AS goals_scored,
                        SUM(CASE
                            WHEN away_team_api_id = %s THEN away_team_goal
                            ELSE home_team_goal
                            END
                        ) AS goals_conceded,
                        COUNT(*) AS all_matches_count
                    FROM match
                    WHERE home_team_api_id = %s OR away_team_api_id = %s
                ) AS subquery
            """, [self.filtered_query_id for _ in range(4)])

            result = cursor.fetchone()
            scored = result[0] or 0
            conceded = result[1] or 0
        return CustomValue(
            text="The average number of goals scored/conceded per match is:"
                 f"{scored:.2f}/{conceded:.2f}",
            value=(scored, conceded),
        )


class ListTeamStatistics(ListBaseStatistics):
    """Class containing methods to generate statistics related to teams."""
    def __init__(self):
        super().__init__()
        self.model = Team
        self.query = Team.objects \
            .values(*get_model_field_names(self.model)) \
            .order_by("team_short_name")

    def display(self) -> List[CustomDataType]:
        """Show all list related statistics"""
        statistics = [
            self.range_by_total_matches_played,
        ]
        return statistics

    @CacheDecoratorFactory.create_decorator()
    def display_list(self) -> ListDataLists:
        """Get all values of related model"""
        return ListDataLists.create_from_queryset(
            queryset=self.query,
            model=self.model
        )

    @CacheDecoratorFactory.create_decorator()
    def range_by_total_matches_played(self) -> Histogram:
        """Calculate the total number of matches played by each team"""
        with connection.cursor() as cursor:
            cursor.execute("""
                SELECT
                    team.team_long_name AS team_name,
                    COUNT(match.match_api_id) AS total_matches_played
                FROM
                    team
                INNER JOIN
                    match ON team.team_api_id IN (match.home_team_api_id,
                        match.away_team_api_id)
                GROUP BY
                    team.team_long_name
                ORDER BY
                    total_matches_played DESC,
                    team_name ASC;
            """)
            team_matches = cursor.fetchall()
        x = [row[0] for row in team_matches]
        y = [row[1] for row in team_matches]

        return Histogram(
            name="The most amount of matches",
            x_axis="Teams names",
            y_axis="Match amount",
            x_data=x,
            y_data=y
        )


class ConcretePlayerStatistics(ConcreteBaseStatistics):
    def __init__(self, id: int) -> None:
        super().__init__(id)
        self.model = Player
        self.id = id
        with connection.cursor() as cursor:
            cursor.execute("""
                SELECT player_api_id
                FROM player
                WHERE id = %s;
            """, [id])
            row = cursor.fetchone()
            if row is not None:
                self.player_api_id = row[0]
            else:
                raise Player.DoesNotExist
        self.query = Player.objects.get(id=self.id)

    def get_current_object_fields(self) -> SingleDataList:
        """Get all fields for concrete statistics"""
        super().get_current_object_fields()
        return SingleDataList.create_from_queryset(
            queryset=self.query,
            model=self.model
        )

    def display(self) -> List[CustomDataType]:
        return {
            self.player_growth,
        }

    def player_growth(self) -> Graph:
        """Retrieve the evolution of player performance ratings over time"""
        with connection.cursor() as cursor:
            cursor.execute("""
                SELECT date, overall_rating
                FROM player_attributes
                WHERE player_api_id = %s
                ORDER BY date;
            """, [self.player_api_id])
            rating_evolution = cursor.fetchall()
        x = [row[0] for row in rating_evolution]
        y = [row[1] for row in rating_evolution]
        return Graph(name="Player's progress",
                     x_axis="Date",
                     y_axis="Overall Rating",
                     x_data=x, y_data=y)


class ListPlayerStatistics(ListBaseStatistics):
    """Class containing methods to generate statistics related to players."""
    def __init__(self):
        super().__init__()
        self.model = Player
        self.query = Player.objects \
            .values(*get_model_field_names(self.model)) \
            .order_by('player_name')

    def display(self) -> List[CustomDataType]:
        """Show all related Player list statistics"""
        statistics = [
            self.overall_rating_distribution,
            self.player_average_rating,
            self.top_goal_scorers,
            self.players_with_most_assists,
            self.players_with_most_clean_sheets
        ]
        return statistics

    @CacheDecoratorFactory.create_decorator()
    def display_list(self) -> ListDataLists:
        """Get all values of related model"""
        return ListDataLists.create_from_queryset(
            queryset=self.query,
            model=self.model)

    @CacheDecoratorFactory.create_decorator()
    def overall_rating_distribution(self) -> Histogram:
        """Calculate the distribution of overall ratings among players."""
        with connection.cursor() as cursor:
            cursor.execute("""
                SELECT overall_rating, COUNT(overall_rating) AS count
                FROM player_attributes
                GROUP BY overall_rating
                ORDER BY overall_rating DESC;
            """)
            rating_distribution = cursor.fetchall()
        x = [row[0] for row in rating_distribution]
        y = [row[1] for row in rating_distribution]
        return Histogram(name="Rating Distribution",
                         x_axis="Rating Value",
                         y_axis="Amount",
                         x_data=x,
                         y_data=y)

    @CacheDecoratorFactory.create_decorator()
    def player_average_rating(self) -> Histogram:
        """Calculate the ratings for each player."""
        # Get a queryset of PlayerAttributes objects annotated with the count
        # of each rating and player name
        with connection.cursor() as cursor:
            cursor.execute("""
                SELECT
                    player.player_name AS player_name,
                    AVG(playerattributes.overall_rating) AS avg_overall_rating
                FROM
                    player_attributes AS playerattributes
                INNER JOIN
                    player AS player
                ON
                    playerattributes.player_api_id = player.player_api_id
                GROUP BY
                    player.player_name
                ORDER BY
                    avg_overall_rating DESC,
                    player.player_name;
            """)
            rating_distribution = cursor.fetchall()
        x = [row[0] for row in rating_distribution]
        y = [row[1] for row in rating_distribution]
        return Histogram(name="The most amount of matches",
                         x_axis="Teams names",
                         y_axis="Match amount",
                         x_data=x, y_data=y)

    @CacheDecoratorFactory.create_decorator()
    def top_goal_scorers(self) -> Histogram:
        """Identify the top goal scorers among players."""
        with connection.cursor() as cursor:
            cursor.execute("""
                SELECT
                    "player"."player_name" AS "player_api__player_name",
                    AVG("player_attributes"."finishing") AS "total_goals"
                FROM
                    "player_attributes"
                INNER JOIN
                    "player"
                ON
                    "player_attributes"."player_api_id" =
                           "player"."player_api_id"
                GROUP BY
                    "player"."player_name"
                ORDER BY
                    "total_goals" DESC;
            """)
            top_scorers = cursor.fetchall()
        x = [row[0] for row in top_scorers]
        y = [row[1] for row in top_scorers]
        return Histogram(name="Top players by score",
                         x_axis="Player Name",
                         y_axis="Overall Score",
                         x_data=x,
                         y_data=y)

    @CacheDecoratorFactory.create_decorator()
    def players_with_most_assists(self) -> Histogram:
        """Players with the highest number of assists, probably."""
        with connection.cursor() as cursor:
            cursor.execute("""
                SELECT
                    player.player_name AS player_name,
                    AVG(player_attributes.short_passing +
                        player_attributes.long_passing +
                        player_attributes.vision +
                        player_attributes.crossing)/5.0 AS
                            assist_coefficient
                FROM
                    player
                JOIN
                    player_attributes ON player.player_api_id =
                           player_attributes.player_api_id
                GROUP BY
                    player.player_name
                ORDER BY
                    assist_coefficient DESC;
            """)
            player_assist = cursor.fetchall()
        x = [row[0] for row in player_assist]
        y = [row[1] for row in player_assist]
        return Histogram(
            name="Top assist players",
            x_axis="Player Names",
            y_axis="Assist Coefficient",
            x_data=x,
            y_data=y
        )

    @CacheDecoratorFactory.create_decorator()
    def players_with_most_clean_sheets(self) -> Histogram:
        """Identify the players (goalkeepers) with the most clean sheets."""
        with connection.cursor() as cursor:
            cursor.execute('''
                SELECT
                    player.player_name AS player_name,
                    AVG(gk_diving+
                        gk_handling +
                        gk_kicking +
                        gk_positioning +
                        gk_reflexes)/5.0 AS cleanliness_coefficient
                FROM
                    player_attributes
                JOIN
                    player ON
                        player.player_api_id = player_attributes.player_api_id
                GROUP BY
                    player.player_name
                ORDER BY
                    cleanliness_coefficient DESC;
            ''')
            player_assist = cursor.fetchall()
        x = [row[0] for row in player_assist]
        y = [row[1] for row in player_assist]
        return Histogram(
            name="Top players with cleanest sheets (probably)",
            x_axis="Player Names",
            y_axis="Cleanliness Coefficient",
            x_data=x,
            y_data=y
        )


class ConcreteLeagueStatistics(ConcreteBaseStatistics):
    """Concrete League statistics"""
    def __init__(self, id):
        super().__init__(id)
        self.model = League
        self.query = League.objects.get(id=self.id)

    def get_current_object_fields(self) -> SingleDataList:
        """Get all fields for concrete statistics"""
        super().get_current_object_fields()
        return SingleDataList.create_from_queryset(
            queryset=self.query,
            model=self.model
        )

    def display(self) -> List[CustomDataType]:
        """Show all related statistics"""
        return [
            self.table_standings,
            self.top_teams_by_points,
            self.goal_difference_distribution,
        ]

    def table_standings(self) -> Histogram:
        """Generate the league table standings for the given league."""
        with connection.cursor() as cursor:
            cursor.execute("""
                SELECT
                    CONCAT(
                        home_team.team_short_name,
                        ' vs ',
                        away_team.team_short_name
                    ) AS match_oposition,
                    GREATEST(0, ABS(m.home_team_goal - m.away_team_goal))
                           AS team_range
                FROM
                    match AS m
                INNER JOIN
                    team AS home_team
                    ON m.home_team_api_id = home_team.team_api_id
                INNER JOIN
                    team AS away_team
                    ON m.away_team_api_id = away_team.team_api_id
                WHERE
                    m.league_id = %s
                ORDER BY
                    match_oposition DESC;
            """, [self.id])
            rating_distribution = cursor.fetchall()
        x = [row[0] for row in rating_distribution]
        y = [row[1] for row in rating_distribution]
        return Histogram(
            name="Leagues Distribution for Most Goal Stangings",
            x_axis="Home VS Away",
            y_axis="Goal Standings",
            x_data=x,
            y_data=y
        )

    def top_teams_by_points(self) -> Histogram:
        """Identify the top teams in terms of points for the given league."""
        with connection.cursor() as cursor:
            cursor.execute('''
                SELECT
                    team.team_long_name AS team_name,
                    SUM(CASE
                            WHEN match.home_team_api_id = team.team_api_id AND
                                match.home_team_goal > match.away_team_goal
                                    THEN match.home_team_goal
                            WHEN match.away_team_api_id = team.team_api_id AND
                                match.away_team_goal > match.home_team_goal
                                    THEN match.away_team_goal
                            ELSE 0
                        END) AS total_points
                FROM
                    team
                JOIN
                    match ON team.team_api_id = match.home_team_api_id OR
                           team.team_api_id = match.away_team_api_id
                WHERE
                    match.id IN %s
                GROUP BY
                    team.team_long_name
                ORDER BY
                    total_points DESC;
            ''', [tuple(Match.objects.filter(league__id=self.id)
                        .values_list('id', flat=True))])
            player_assist = cursor.fetchall()

            x = [row[0] for row in player_assist]
            y = [row[1] for row in player_assist]
        return Histogram(
            name="Top teams by points",
            x_axis="Team Name",
            y_axis="Points",
            x_data=x,
            y_data=y
        )

    def goal_difference_distribution(self) -> Histogram:
        """Calculate the distribution of goal differences across teams"""
        with connection.cursor() as cursor:
            cursor.execute('''
                SELECT
                    team_range AS team_range,
                    COUNT(team_range) AS diff_distribution
                FROM (
                    SELECT
                        CASE
                            WHEN (home_team_goal - away_team_goal) > 0
                                THEN (home_team_goal - away_team_goal)
                            WHEN (away_team_goal - home_team_goal) > 0
                                THEN (away_team_goal - home_team_goal)
                            ELSE 0
                        END AS team_range
                    FROM
                        match
                    INNER JOIN
                        league ON match.league_id = league.id
                    WHERE
                        league.id = %s
                ) AS subquery
                GROUP BY
                    team_range
                ORDER BY
                    team_range DESC;
            ''', [self.id])
            difference = cursor.fetchall()
        x = [row[0] for row in difference]
        y = [row[1] for row in difference]
        return Histogram(
            name="Distribution of goal difference",
            x_axis="Value of Difference",
            y_axis="Count of Difference",
            x_data=x,
            y_data=y
        )


class ListLeagueStatistics(ListBaseStatistics):
    """Class containing methods to generate statistics related to leagues"""
    def __init__(self):
        super().__init__()
        self.model = League
        self.query = League.objects.all() \
            .order_by('-name') \
            .values(*get_model_field_names(self.model))

    def display(self) -> List[CustomDataType]:
        """Show all related statistics"""
        return [
            self.most_matches_played
        ]

    @CacheDecoratorFactory.create_decorator()
    def display_list(self) -> ListDataLists:
        """Get all values of related model"""
        return ListDataLists.create_from_queryset(
            queryset=self.query,
            model=self.model
        )

    @CacheDecoratorFactory.create_decorator()
    def most_matches_played(self) -> Histogram:
        with connection.cursor() as cursor:
            cursor.execute('''
                SELECT
                    league.name AS league_name,
                    COUNT(match.id) AS match_count
                FROM
                    league
                JOIN
                    match ON league.id = match.league_id
                GROUP BY
                    league.name
                ORDER BY
                    match_count;
            ''')
            player_assist = cursor.fetchall()

            x = [row[0] for row in player_assist]
            y = [row[1] for row in player_assist]
        return Histogram(name="Most Matches Played",
                         x_axis="Leagues Names",
                         y_axis="Count of matches taken",
                         x_data=x,
                         y_data=y)


class ConcreteMatchStatistics(ConcreteBaseStatistics):
    def __init__(self, id):
        super().__init__(id)
        self.model = Match
        self.query = Match.objects \
            .values(*get_model_field_names(self.model)) \
            .get(pk=self.id)

    def get_current_object_fields(self) -> SingleDataList:
        """Get all fields for concrete statistics"""
        super().get_current_object_fields()
        return SingleDataList.create_from_queryset(
            queryset=self.query,
            model=self.model
        )

    def display(self) -> List[CustomDataType]:
        """Show all related statistics"""
        return []


class ListMatchStatistics(ListBaseStatistics):
    """
    Class containing methods to generate statistics related to matches.
    """
    def __init__(self):
        super().__init__()
        self.model = Match
        self.query = Match.objects \
            .values(*get_model_field_names(self.model)) \
            .order_by('-date')

    def display(self) -> List[CustomDataType]:
        """Show all related statistics"""
        return [
            self.avg_goals_per_match,
            self.goal_distribution_in_time_intervals,
            self.matches_with_most_goals,
        ]

    @CacheDecoratorFactory.create_decorator()
    def display_list(self) -> ListDataLists:
        """Get all values of related model"""
        return ListDataLists.create_from_queryset(
            queryset=self.query,
            model=self.model
        )

    @CacheDecoratorFactory.create_decorator()
    def avg_goals_per_match(self) -> CustomValue:
        """Calculate the average goals per match."""
        with connection.cursor() as cursor:
            cursor.execute('''
                SELECT AVG(home_team_goal + away_team_goal) AS
                           average_goals_per_match
                FROM match;
            ''')
            row = cursor.fetchone()
        value = row[0] if row else None
        return CustomValue(
            text=f"Average value of goals per match is {value}",
            value=value)

    @CacheDecoratorFactory.create_decorator()
    def goal_distribution_in_time_intervals(self) -> Graph:
        """Calculate distribution of goals scored in different intervals."""
        with connection.cursor() as cursor:
            cursor.execute('''
                SELECT
                    EXTRACT(YEAR FROM match.date::date) AS year,
                    AVG(away_team_goal + home_team_goal) AS average
                FROM
                    match
                GROUP BY
                    year;
            ''')
            distribution = cursor.fetchall()

        x = [int(row[0]) for row in distribution]
        y = [float(row[1]) for row in distribution]
        return Histogram(name="Goals Distribution in Time Intervals",
                         x_axis="Year",
                         y_axis="Average Number of Goals",
                         x_data=x,
                         y_data=y)

    @CacheDecoratorFactory.create_decorator()
    def matches_with_most_goals(self) -> Graph:
        """Identify matches with the most goals."""
        with connection.cursor() as cursor:
            cursor.execute('''
                WITH match_data AS (
                    SELECT
                        match.home_team_api_id,
                        match.away_team_api_id,
                        match.home_team_goal + match.away_team_goal
                           AS total_goals,
                        match.date::date AS match_date,
                        EXTRACT(YEAR FROM match.date::date) AS match_year,
                        ROW_NUMBER() OVER (PARTITION BY EXTRACT(YEAR FROM
                           match.date::date) ORDER BY match.home_team_goal +
                           match.away_team_goal DESC) AS rn
                    FROM
                        match
                )
                SELECT
                    CONCAT(match_year, ') ', home_team.team_short_name, ' vs ',
                           away_team.team_short_name) AS match_teams,
                    total_goals,
                    match_year
                FROM
                    match_data
                JOIN
                    team AS home_team ON match_data.home_team_api_id =
                           home_team.team_api_id
                JOIN
                    team AS away_team ON match_data.away_team_api_id =
                           away_team.team_api_id
                WHERE
                    rn = 1
                ORDER BY
                    match_year, total_goals DESC;
            ''')
            player_assist = cursor.fetchall()

            x = [row[0] for row in player_assist]
            y = [row[1] for row in player_assist]
        return Histogram(name="Matches with the most goals",
                         x_axis="Year",
                         y_axis="Average amount of goals",
                         x_data=x,
                         y_data=y)


class ConcreteCountryStatistics(ConcreteBaseStatistics):
    def __init__(self, id):
        super().__init__(id)
        self.model = Country
        self.query = Country.objects.get(id=self.id)

    def get_current_object_fields(self) -> SingleDataList:
        """Get all fields for concrete statistics"""
        super().get_current_object_fields()
        return SingleDataList.create_from_queryset(
            queryset=self.query,
            model=self.model
        )

    def display(self) -> List[CustomDataType]:
        """Show all related statistics"""
        return []


class ListCountryStatistics(ListBaseStatistics):
    """
    Class containing methods to generate statistics related to countries.
    """
    def __init__(self):
        super().__init__()
        self.model = Country

    def display(self) -> List[CustomDataType]:
        """Show all related statistics"""
        return [
            self.overall_performance_comparison,
        ]

    @CacheDecoratorFactory.create_decorator()
    def display_list(self) -> ListDataLists:
        """Get all values of related model"""
        return ListDataLists.create_from_queryset(
            queryset=Country.objects.all()
            .values(*get_model_field_names(self.model))
            .order_by('name'),
            model=self.model
        )

    @CacheDecoratorFactory.create_decorator()
    def overall_performance_comparison(self) -> Histogram:
        """Compare overall performance between countries."""
        with connection.cursor() as cursor:
            cursor.execute('''
                SELECT
                    c.name AS country_name,
                    SUM(m.home_team_goal + m.away_team_goal) AS total_goals
                FROM
                    match AS m
                INNER JOIN
                    country AS c ON m.country_id = c.id
                GROUP BY
                    c.name
                ORDER BY
                    c.name;
            ''')

            results = cursor.fetchall()
        x = [row[0] for row in results]
        y = [row[1] for row in results]
        return Histogram(
            name="Average goals for countries",
            x_axis="Country Name",
            y_axis="Average Goals Amount",
            x_data=x,
            y_data=y
        )


class AbstractStatisticsFactory(Protocol):
    """Abstract Factory for creating statistics classes"""
    def create_statistics(self, stats_type: str) -> BaseStatistics:
        """Factory method to create statistics objects"""
        pass

    def prepare_groups(self):
        """Create contatiner divided by data for response"""
        pass


class ListStatisticsFactory(AbstractStatisticsFactory):
    """Concrete Factory for creating statistics classes"""
    def __init__(self, stats_type: str):
        self.related_objects = self.create_statistics(stats_type)

    def create_statistics(self, stats_type: str) -> ListBaseStatistics:
        """Factory method to create statistics objects"""
        return {
            "country": ListCountryStatistics,
            "league": ListLeagueStatistics,
            "team": ListTeamStatistics,
            "player": ListPlayerStatistics,
            "match": ListMatchStatistics
        }[stats_type]()

    def prepare_list(self):
        return self.related_objects.display_list()

    def prepare_graphs(self):
        return self.related_objects.display()

    def prepare_groups(self) -> ListViewFields:
        """Create container of texts, plot data, list of data"""
        name = self.related_objects.model._meta.verbose_name \
            .replace('_', ' ').capitalize()
        stats = self.prepare_graphs()
        entrance, visual = list(), list()
        for item in stats:
            stats_value = item()
            if stats_value.page_part is PageParts.ENTRANCE:
                entrance.append(stats_value)
            elif stats_value.page_part is PageParts.VISUAL:
                visual.append(stats_value)
        queryset = self.prepare_list()
        return ListViewFields(name=name,
                              entrance=entrance,
                              visual=visual,
                              queryset=queryset)


class ConcreteStatisticsFactory(AbstractStatisticsFactory):
    """Concrete Factory for creating statistics classes"""
    def __init__(self,  stats_type: str, id: int):
        self.related_objects = self.create_statistics(stats_type, id)
        self.id = id

    def create_statistics(self,
                          stats_type: str,
                          id: int) -> ConcreteBaseStatistics:
        """Factory method to create statistics objects"""
        return {
            "country": ConcreteCountryStatistics,
            "league": ConcreteLeagueStatistics,
            "team": ConcreteTeamStatistics,
            "player": ConcretePlayerStatistics,
            "match": ConcreteMatchStatistics
        }[stats_type](id)

    def get_fields(self):
        return self.related_objects.get_current_object_fields()

    def prepare_graphs(self):
        return self.related_objects.display()

    def prepare_groups(self) -> SingleViewFields:
        """Create container of texts and plot data"""
        name = self.related_objects.model._meta.verbose_name \
            .replace('_', ' ').capitalize()
        stats = self.prepare_graphs()
        entrance, visual = list(), list()
        for item in stats:
            stats_value = item()
            if stats_value.page_part is PageParts.ENTRANCE:
                entrance.append(stats_value)
            elif stats_value.page_part is PageParts.VISUAL:
                visual.append(stats_value)
        fields = self.get_fields()
        return SingleViewFields(name=name,
                                entrance=entrance,
                                visual=visual,
                                fields=fields)

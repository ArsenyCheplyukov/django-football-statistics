"""
Views for statistics application (fullstack and backend)
"""
# base python imports
from django.db.models import QuerySet
from .datatypes import PageParts

# django imports
from django.shortcuts import render
from django.http import (
    Http404,
    HttpResponseNotFound,
)


# imports for DRF
from drf_spectacular.utils import (extend_schema_view,
                                   extend_schema,
                                   OpenApiParameter,
                                   OpenApiTypes)
from rest_framework import (
    status,
)
from rest_framework import generics
from rest_framework.response import Response

# import custom paginator
from .paginator import Paginator

# main local things
from .queries import ConcreteStatisticsFactory, ListStatisticsFactory
from .serializers import (GraphSerializer,
                          SingleDataListSerializer,
                          ListDataListsSerializer,
                          CustomValueSerializer)

from .models import (
    Country, League, Team, Player, Match
)


MODEL_IS_NOT_EXIST_ERRORS = (
    Country.DoesNotExist,
    League.DoesNotExist,
    Team.DoesNotExist,
    Player.DoesNotExist,
    Match.DoesNotExist
)


# DJANGO REST FRAMEWORK VIEWS
@extend_schema_view(
    list=extend_schema(
        parameters=[
            OpenApiParameter(
                'name',
                OpenApiTypes.STR,
                description='Name of the statistics',
                location=OpenApiParameter.PATH
            )
        ]
    )
)
class ListView(generics.ListAPIView):
    """Show list of generic data"""
    def get_queryset(self):
        stats_type: str = self.kwargs.get('name')
        factory = ListStatisticsFactory(stats_type)
        queryset = factory.prepare_list()
        return queryset

    def list(self, request, *args, **kwargs):
        try:
            queryset = self.get_queryset()
            serializer = ListDataListsSerializer(queryset)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except IndexError:
            return Response({"error": "Index out of range"},
                            status=status.HTTP_404_NOT_FOUND)
        except KeyError:
            return Response({"error": "Key is not found"},
                            status=status.HTTP_404_NOT_FOUND)
        except AttributeError:
            return Response({"error": "This name is wrong"},
                            status=status.HTTP_404_NOT_FOUND)


@extend_schema_view(
    list=extend_schema(
        parameters=[
            OpenApiParameter(
                'name',
                OpenApiTypes.STR,
                description='Name of the statistics',
                location=OpenApiParameter.PATH
            ),
            OpenApiParameter(
                'query_id',
                OpenApiTypes.INT,
                description='ID of the query',
                location=OpenApiParameter.PATH
            )
        ]
    )
)
class QueryListView(generics.RetrieveAPIView):
    """Abstract base class for list views."""
    def get_object(self) -> QuerySet:
        stats_type: str = self.kwargs.get('name')
        query_id: int = int(self.kwargs.get('query_id'))
        factory = ListStatisticsFactory(stats_type)
        queryset: QuerySet = factory.prepare_graphs()[query_id]()
        return queryset

    def retrieve(self, request, *args, **kwargs):
        try:
            queryset = self.get_object()
            if queryset.page_part is PageParts.VISUAL:
                serializer = GraphSerializer(queryset)
            elif queryset.page_part is PageParts.ENTRANCE:
                serializer = CustomValueSerializer(queryset)
            else:
                return Response(status=status.HTTP_404_NOT_FOUND)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except Http404:
            return Response({"error": "Query ID not found"},
                            status=status.HTTP_404_NOT_FOUND)
        except IndexError:
            return Response({"error": "Index out of range"},
                            status=status.HTTP_404_NOT_FOUND)
        except KeyError:
            return Response({"error": "name is not valid"},
                            status=status.HTTP_404_NOT_FOUND)
        except AttributeError:
            return Response({"error": "Name is not valid"},
                            status=status.HTTP_404_NOT_FOUND)


@extend_schema_view(
    list=extend_schema(
        parameters=[
            OpenApiParameter(
                'name',
                OpenApiTypes.STR,
                description='Name of the statistics',
                location=OpenApiParameter.PATH
            ),
            OpenApiParameter(
                'id',
                OpenApiTypes.INT,
                description='ID of the statistics',
                location=OpenApiParameter.PATH
            )
        ]
    )
)
class HomeView(generics.ListAPIView):
    """Serialization class for home views."""
    def get_object(self):
        stats_type = self.kwargs.get('name')
        id = int(self.kwargs.get('id'))
        factory = ConcreteStatisticsFactory(stats_type, id)
        queryset = factory.get_fields()
        return queryset

    def list(self, request, *args, **kwargs):
        try:
            queryset = self.get_object()
            # Create an instance of the serializer
            serializer = SingleDataListSerializer(queryset)
        except Http404:
            return Response({"error": "Query ID not found"},
                            status=status.HTTP_404_NOT_FOUND)
        except IndexError:
            return Response({"error": "Index out of range"},
                            status=status.HTTP_404_NOT_FOUND)
        except KeyError:
            return Response({"error": "Name is not valid"},
                            status=status.HTTP_404_NOT_FOUND)
        except MODEL_IS_NOT_EXIST_ERRORS:
            return Response({"error": "Object does not exists"},
                            status=status.HTTP_404_NOT_FOUND)
        # Return the serialized data
        return Response(serializer.data, status=status.HTTP_200_OK)


@extend_schema_view(
    list=extend_schema(
        parameters=[
            OpenApiParameter(
                'name',
                OpenApiTypes.STR,
                description='Name of the statistics',
                location=OpenApiParameter.PATH
            ),
            OpenApiParameter(
                'id',
                OpenApiTypes.INT,
                description='ID of the statistics',
                location=OpenApiParameter.PATH
            ),
            OpenApiParameter(
                'query_id',
                OpenApiTypes.INT,
                description='ID of the query',
                location=OpenApiParameter.PATH
            )
        ]
    )
)
class QueryHomeView(generics.RetrieveAPIView):
    def get_object(self) -> QuerySet:
        stats_type: str = self.kwargs.get('name')
        id: int = int(self.kwargs.get('id'))
        query_id: int = int(self.kwargs.get('query_id'))
        factory = ConcreteStatisticsFactory(stats_type, id)
        queryset: QuerySet = factory.prepare_graphs()[query_id]()
        return queryset

    def retrieve(self, request, *args, **kwargs):
        try:
            queryset = self.get_object()
            if queryset.page_part is PageParts.VISUAL:
                serializer = GraphSerializer(queryset)
            elif queryset.page_part is PageParts.ENTRANCE:
                serializer = CustomValueSerializer(queryset)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except Http404:
            return Response({"error": "Query ID not found"},
                            status=status.HTTP_404_NOT_FOUND)
        except IndexError:
            return Response({"error": "Index out of range"},
                            status=status.HTTP_404_NOT_FOUND)
        except KeyError:
            return Response({"error": "Name is not valid"},
                            status=status.HTTP_404_NOT_FOUND)
        except TypeError:
            return Response({"error": "Invalid type"},
                            status=status.HTTP_404_NOT_FOUND)
        except MODEL_IS_NOT_EXIST_ERRORS:
            return Response({"error": "Object does not exists"},
                            status=status.HTTP_404_NOT_FOUND)


# VANILA DJANGO VIEWS
def list_view(request, name: str):
    """Generalized approach of creating list views"""
    try:
        # Retrieve the queryset from ListStatisticsFactory
        statistics = ListStatisticsFactory(name).prepare_groups()
        name = statistics.name
        entrance = statistics.entrance
        visual = statistics.visual
        queryset = statistics.queryset

        # Paginate the queryset
        paginator = Paginator(queryset.data)
        page_number = int(request.GET.get('page') or 0)
        paginated_queryset = paginator.get_page(page_number)

        return render(request, 'list_view.html', {
            'queryset': paginated_queryset['data'],
            'paginator': paginated_queryset,
            'name': name,
            'entrance': entrance,
            'visual': visual,
        })
    except Exception as e:  # noqa
        # Handle other exceptions
        return HttpResponseNotFound(render(request, 'error_404.html'))


def concrete_view(request, name: str, id: int):
    """Generalized approach of creating concrete type views"""
    try:
        statistics = ConcreteStatisticsFactory(name, id).prepare_groups()
        return render(request, 'home_view.html', {'main': statistics,
                                                  'name': name,
                                                  'id': id})
    except Exception:
        # Handle other exceptions
        return HttpResponseNotFound(render(request, 'error_404.html'))


def home(request):
    return render(request, 'home.html')
